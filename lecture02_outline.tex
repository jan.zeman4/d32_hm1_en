\documentclass{MHM1_lectures}

\newcommand{\vek}[1]{{\boldsymbol #1}}
\newcommand{\phs}[1]{^{(#1)}}
\newcommand{\fl}{'} % Fluctuation

\begin{document}
    
\maketitle{2}{Variational principles and Helmholtz decomposition}

\tableofcontents

\section{Energy potentials}

\begin{itemize}

\item Primal and dual local energy potentials $w : \Omega \times \mathbb{R}^d \rightarrow \mathbb{R}$, $w^* : \Omega \times \mathbb{R}^d \rightarrow \mathbb{R}$ 
%
\begin{align*}
    w( \vek{x}, \vek{s} )
    = 
    \sum_{r=1}^N
    \chi\phs{r}( \vek{x} ) w\phs{r}( \vek{s} ), 
    && 
    w^*( x, \vek{t} ) 
    = 
    \sum_{r=1}^N
    \chi\phs{r}( \vek{x} ) {w^*}\phs{r}( \vek{t} )
\end{align*}
%
where 
%
\begin{align*}
    w\phs{r}( \vek{s} ) 
    = 
    \frac{1}{2} \vek{s}^T \vek{L}\phs{r} \vek{s}, &&
    {w^*}\phs{r}( \vek{t} ) = \frac{1}{2} \vek{t}^T \vek{M}\phs{r} \vek{t}, &&
    \vek{s}, \vek{t} \in \mathbb{R}^d
\end{align*}

\item Then, the constitute relations allow for an equivalent formulation
%
\begin{align*}
\vek{\jmath} (\vek{x} )
& =
\frac{%
\partial 
w( \vek{x}, \vek{s})
}{%
\partial \vek{t}
}
\bigl( \vek{e}( \vek{x}) \bigr),
&
\vek{e} (\vek{x} )
& =
\frac{%
\partial 
w^*( \vek{x}, \vek{t})
}{%
\partial \vek{t}
}
\bigl( \vek{\jmath}( \vek{x}) \bigr),
\\
\vek{L} (\vek{x} )
& =
\frac{%
\partial^2 
w( \vek{x}, \vek{s})
}{%
\partial \vek{s}^2
},
& 
\vek{M} (\vek{x} )
& =
\frac{%
\partial^2 
w^*( \vek{x}, \vek{t})
}{%
\partial \vek{t}^2
}.
\end{align*}

\item Thereby $\vek{L}^{(r)}$ and $\vek{M}^{(r)}$ are symmetric.

\end{itemize}

\section{Variational principles}

\begin{itemize}

    \item Primal energy functional $W: \mathcal{E} \rightarrow \mathbb{R}$
    %
    \begin{align*}
        W( \vek{v} )
        =
        \int_{\Omega}
        w \bigl(x, \vek{v}( \vek{x}) \bigr)
        d \vek{x},
    \end{align*}
    %
    \begin{align*}
        W( \vek{e} ) < W( \vek{v} )
        \text{ for all }
        \vek{v} \neq \vek{e}
        \text{ provided that }
        w^{(r)}( \vek{s} ) > 0 
        \text{ for all }
        \vek{s} \neq \vek{0}
    \end{align*}

    \item Dual energy functional $W^* : \mathcal{J} \rightarrow \mathbb{R}$
    %
    \begin{align*}
        W^*( \vek{q} )
        =
        \int_{\Omega}
        w^* \bigl(x, \vek{q}( \vek{x}) \bigr)
        d \vek{x},
    \end{align*}
    %
    \begin{align*}
        W^*( \vek{\jmath} ) \leq W^*( \vek{q} )
        \text{ for all }
        \vek{q} \neq 
        \vek{\jmath}
        \text{ provided that }
        w^{(r)}( \vek{t} ) > 0 
        \text{ for all }
        \vek{t} \neq \vek{0}
    \end{align*}

    \item Requires $\vek{L}^{(r)}$ and $\vek{M}^{(r)}$ to be positive definite
    %
    \begin{align*}
        \vek{L}^{(r)} \succ \vek{0}, 
        &&
        \vek{M}^{(r)} \succ \vek{0}.
    \end{align*}

\end{itemize}

\section{Orthogonality}

\begin{itemize}
    \item In $\mathbb{R}^2$: $\vek{s}^T \vek{t} = 0$, $\mathbb{R}^2 = \mathbb{S} \oplus \mathbb{T}$
    \item For vector fields: $\vek{f}: \Omega \rightarrow \mathbb{R}^d$, $\vek{g}: \Omega \rightarrow \mathbb{R}^d$ 
    %
    \begin{align*}
        ( \vek{f}, \vek{g} )
        =
        \int_{\Omega} 
        \vek{f}( \vek{x} )^T \vek{g}( \vek{x} )
        d \vek{x}
        =
        0  
    \end{align*}

\end{itemize}

\paragraph{Examples}

\begin{itemize}
    \item Average value $\langle \vek{f}( \vek{x} ) \rangle$
    \item Fluctuation of $\vek{f}\fl$
    \item Constant and zero-mean-value fields: $( \vek{c}, \vek{f}\fl ) = 0$ for any $\vek{c} \in \mathbb{R}^d$ and fluctuating $\vek{f}\fl$
    \item Two fields $\vek{v} \in \mathcal{E}$ and $\vek{q} \in \mathcal{J}$
%
\begin{align*}
    ( \vek{v}, \vek{q} ) 
    =
    \int_{\partial \Omega}
    \varphi( \vek{x} ) \vek{n}^T( \vek{x} ) \vek{w}( \vek{x} ) 
    d \vek{x}
    =
    0
\end{align*} 
%
can be satisfied for $\varphi( \vek{x} ) = 0$ on $\partial \Omega$ or $\vek{n}^T( \vek{x} ) \vek{q}( \vek{x} ) = 0$ on $\partial \Omega$. 

\end{itemize}

\section{Helmholtz decomposition}
%
\begin{itemize}

\item For finite $\Omega$

\begin{align*}
    \mathcal{H}_{\Omega} 
    = 
    \mathbb{R}^d 
    \oplus 
    \mathcal{E}_{\Omega, \mathrm{D}}\fl 
    \oplus 
    \mathcal{J}_{\Omega}\fl
    =
    \mathbb{R}^d 
    \oplus 
    \mathcal{E}_{\Omega}\fl
    \oplus 
    \mathcal{J}_{\Omega, \mathrm{D}}\fl
\end{align*}

\item For infinite $\Omega \equiv \mathbb{R}^d$
%
\begin{align*}
    \mathcal{H}_{\mathbb{R}^d} 
    = 
    \mathbb{R}^d 
    \oplus 
    \mathcal{E}\fl_{\mathbb{R}^d} 
    \oplus 
    \mathcal{J}\fl_{\mathbb{R}^d}
\end{align*}

\item In micromechanics, often referred to as Hill lemma~\cite{Hill:1963:EPS}

\end{itemize}

\paragraph{Homework ideas}

\begin{itemize}\noitemsep
    \item material symmetry elements (take from Torquato)
\end{itemize}

\Reference

\Acknowledgement 

\end{document}