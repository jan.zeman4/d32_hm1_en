\documentclass{MHM1_lectures}

\newcommand{\vek}[1]{{\boldsymbol #1}}
\newcommand{\phs}[1]{^{(#1)}}
\newcommand{\fl}{'} % Fluctuation
\newcommand{\de}{d}
\newcommand{\ft}[1]{\widehat{#1}}
\newcommand{\imu}{\mathrm{i}\,}
\newcommand{\trn}{^T}

\begin{document}
    
\maketitle{8}{Quantification of random microstructures}

\tableofcontents

\section{Motivation}

\begin{itemize}\noitemsep
    \item Effective media theories may lead to nonphysical predictions
    %
    \begin{itemize}\noitemsep
        \item violation of Voigt-Reuss bounds (dilute approximation; in homework)
        \item non-symmetric $\vek{L}^\mathrm{eff}$~(self-consistent, Mori-Tanaka; not shown)
    \end{itemize}

    \item Unclear relation to \emph{uncertainties} in properties and spatial arrangement of heterogeneous materials 
    
    \item We focus on the latter under \emph{minimalist assumptions}
    
    \item More details can be found, e.g., in monographs by Beran~\cite{Beran1968} and Torquato~\cite{Torquato2002} (less rigorous) or Chiu et al.~\cite{Chiu2013} (rigorous)
\end{itemize}

\section{Concept of ensemble (averaging)}

\begin{center}
    \includegraphics{figures/08-ensemble}
\end{center}  

\begin{itemize}\noitemsep
    \item Ensemble~(sample) space $\mathcal{S}$, samples $\alpha \in \mathcal{S}$ with 
    probability $\mu (\alpha)$

    \item Sample properties satisfy $\mu( \alpha ) \geq 0$ and $\sum_{\alpha \in S} \mu( \alpha ) = 1$
    
    \item Domain occupied by the $r$-th phase
    %
    \begin{align*}
        \Omega\phs{r}( \alpha ) \subset \mathbb{R}^d, \quad
        r = 1, \ldots, N, \quad 
        \alpha \in \mathcal{S}    
    \end{align*}
    
    \item Ensemble average of (two-point) random function $f : \mathbb{R}^d \times \mathbb{R}^d \times \mathcal{S} \rightarrow \mathbb{R}$
    %
    \begin{align*}
        \mathbb{E}\bigl[ f( \vek{x}, \vek{y}, \alpha )\bigr]
        =
        \sum_{\alpha \in \mathcal{S}}
        f( \vek{x}, \vek{y}; \alpha )
        \mu( \alpha )
    \end{align*}

    \item Statistical descriptors: Ensemble averages of suitably chosen microstructure function and its (low-order) moments
\end{itemize}

\section{One and two-point probability functions}

\begin{itemize}\noitemsep
    \item Phase characteristic function $\chi\phs{r}: \mathbb{R}^d \times \mathcal{S} \rightarrow \{ 0, 1\}$
    %
    \begin{align*}
        \chi\phs{r}( \vek{x}; \alpha )
        =
        \begin{cases}
            1 & \text{for } \vek{x} \in \Omega\phs{r}( \alpha ), \\
            0 & \text{otherwise},
        \end{cases}
        \quad
        \text{for } r = 1, \ldots, N
    \end{align*}

    \item Two useful identifies hold for $\vek{x} \in \mathbb{R}^d$ and $\alpha \in \mathcal{S}$:
    %
    \begin{align}
        \sum_{r=1}^{N}
        \chi\phs{r}( \vek{x}; \alpha )
        & =
        1
        \text{ for all }
        r = 1, \ldots, N
        \label{eq:chi_identity} \\
        \chi\phs{r}( \vek{x}; \alpha ) 
        \chi\phs{s}( \vek{x}; \alpha )
        & =
        \delta_{rs}
        \text{ for all }
        r, s = 1, \ldots, N
        \label{eq:Kronecker}
    \end{align}

    \item One-point probability function of phase $r$, $S\phs{r} : \mathbb{R}^d \rightarrow [0, 1]$
    %
    \begin{align*}
        S\phs{r}( \vek{x} )
        =
        \mathbb{E} \bigl[ \chi\phs{r}( \vek{x}; \alpha ) \bigr]
        =
        \mathbb{P} \bigl[ \vek{x} \text{ in $r$-th phase}\bigr], 
        \quad 
        r = 1, \ldots, N
    \end{align*}

    \item One-point function of, e.g., matrix phase
    %
    \begin{align*}
        S\phs{1}( \vek{x} )
        =
        \mathbb{E} \bigl[ \chi\phs{1}( \vek{x}; \alpha ) \bigr]
        \stackrel{\eqref{eq:chi_identity}}{=}
        \mathbb{E} \bigl[ 1 - \sum_{r=2}^N \chi\phs{r}( \vek{x}; \alpha ) \bigr]
        =
        \mathbb{E} \bigl[ 1 \bigr]
        -
        \sum_{r=2}^{N} \mathbb{E}\bigl[ \chi\phs{r}( \vek{x}; \alpha ) \bigr]
        =
        1 - \sum_{r=2}^{N} S\phs{r}( \vek{x} )
    \end{align*}

    \item Two-point probability function of phases $r$ and $s$, 
    $S\phs{rs}: \mathbb{R}^d \times \mathbb{R}^d \rightarrow [0, 1]$
    %
    \begin{align*}
        S\phs{rs}( \vek{x}, \vek{y})
        =
        \mathbb{E} \bigl[ 
            \chi\phs{r}( \vek{x}; \alpha ) \chi\phs{s}( \vek{y}; \alpha ) 
        \bigr]
        =
        \mathbb{P} \bigl[ 
            \vek{x} \text{ in $r$-th phase}
            \land 
            \vek{y} \text{ in $s$-th phase}       
        \bigr], 
    \end{align*}

    \item Two-point probability function of matrix phase
    %
    \begin{align*}
        S\phs{11}( \vek{x}, \vek{y})
        & =
        \mathbb{E} \bigl[ 
            \chi\phs{1}( \vek{x}; \alpha ) \chi\phs{1}( \vek{y}; \alpha ) 
        \bigr]
        =
        \mathbb{E} \left[ 
            \bigl( 1 - \sum_{r=2}^{N} \chi\phs{r}( \vek{x}; \alpha ) \bigr) 
            \bigl( 1 - \sum_{s=2}^{N} \chi\phs{s}( \vek{y}; \alpha ) \bigr) 
        \right]
        \\
        & = 
        1 
        - 
        \sum_{r=2}^{N} \bigl( S\phs{r}(\vek{x}) + S\phs{r}( \vek{y} ) \bigr) 
        + 
        \sum_{r,s=2}^{N} S\phs{rs}( \vek{x}, \vek{y})      
    \end{align*}

    \item $\{ S\phs{r} \}_{r=1}^{N} \subset \{ S\phs{rs} \}_{r,s=1}^{N}$
    %
    \begin{align*}
        S\phs{rs}( \vek{x}, \vek{x} )
        =
        \mathbb{E} \bigl[ 
            \chi\phs{r}( \vek{x}; \alpha ) \chi\phs{s}( \vek{x}; \alpha ) 
        \bigr]
        \stackrel{\eqref{eq:Kronecker}}{=}
        \delta_{rs} \, S\phs{r}( \vek{x} ) 
        \text{ (no sum) }
    \end{align*}

    \item Introduced first in a related context by Brown~\cite{Brown1955}.

\end{itemize}

\section{Homogeneity, (ellipsoidal) isotropy, and ergodicity}

\begin{itemize}\noitemsep
    \item \emph{Statistically homogeneous} material/medium: statistics independent of choice of coordinate system~(translation-invariant): For any $\vek{z} \in \mathbb{R}^d$
    %
    \begin{align*}
        S\phs{r}( \vek{x} ) & 
        = S\phs{r}( \vek{x} + \vek{z} )
        \stackrel{\vek{z} = - \vek{x}}{=} 
        S\phs{r}( \vek{0} ) =: S\phs{r}
        && 
        \text{for } r = 1, \ldots, N 
        \\
        S\phs{rs}( \vek{x},  \vek{y}) 
        & 
        = S\phs{rs}( \vek{x} + \vek{z}, \vek{y} + \vek{z} )
        \stackrel{\vek{z} = - \vek{x}}{=}
        = S\phs{rs}( \vek{0}, \vek{y} - \vek{x} )
        = S\phs{rs}( \vek{x} - \vek{y} )
        && \text{for } r, s = 1, \ldots, N
    \end{align*} 

    \item Statistically homogeneous material with \emph{no long-range order} effects~\cite{Willis1977}
    %
    \begin{align*}
        \lim_{\| \vek{x} \| \rightarrow \infty} 
        S\phs{rs}( \vek{x} )
        =
        S\phs{r} S\phs{s}
        \text{ for }
        r,s = 1, \ldots, N
    \end{align*}

    \item Material/medium with \emph{ellipsoidal isotropy}~\cite{Willis1977} 
    %
    \begin{align*}
        \vek{A} = \vek{A}\trn \in \mathbb{R}^{d \times d},
        &&
        \vek{A} \succ \vek{0},
        &&
        \| \vek{x} \|_{\vek{A}}^2 
        =
        \vek{x}\trn \vek{A} \vek{x}
    \end{align*}
    %
    \begin{align*}
        S\phs{rs}( \vek{x} ) = S\phs{rs}( \| \vek{x} \|_{\vek{A}} )
    \end{align*}

    \item Ergodic medium~(requires homogeneity)
    %
    \begin{align*}
        S\phs{rs}( \vek{x} )
        =
        \langle 
            \chi\phs{r}( \vek{y}; \alpha )
            \chi\phs{s}( \vek{y} + \vek{x}; \alpha ) 
        \rangle_{\vek{y}}
        =
        \lim_{| \Omega | \rightarrow \infty}
        \frac{1}{| \Omega |}
        \int_{\Omega}
        \chi\phs{r}( \vek{y}; \alpha )
        \chi\phs{s}( \vek{y} + \vek{x}; \alpha )
        \de\vek{y}
    \end{align*}
    %
    for any $\alpha \in \mathcal{S}$.

    \item In particular, for any $\alpha \in \mathcal{S}$
    %
    \begin{align*}
        S\phs{rr}( \vek{0} ) 
        =
        S\phs{r}
        & =
        \lim_{| \Omega | \rightarrow \infty}
        \frac{1}{| \Omega |}
        \int_{\Omega}
        \chi\phs{r}( \vek{y}; \alpha )
        \chi\phs{s}( \vek{y}; \alpha )
        \de\vek{y}
        =
        \lim_{| \Omega | \rightarrow \infty}
        \frac{1}{| \Omega |}
        \int_{\Omega}
        \chi\phs{r}( \vek{y}; \alpha )
        \de\vek{y}
        \\
        & =
        \lim_{| \Omega | \rightarrow \infty}
        \frac{| \Omega\phs{r}( \alpha ) |}{| \Omega |}        
        =
        c\phs{r}
    \end{align*}

    \item Example for $N = 2$ (more via \url{https://mulibweb.azurewebsites.net})

\end{itemize}

\smallskip
\noindent
\begin{tabular}{ccc}
    & $c\phs{1} = 79.7\%$ & $c\phs{2} = 20.3\%$ \\
    \includegraphics[height=47.5mm]{figures/08-example-microstructure} &
    \includegraphics[height=47.5mm]{figures/08-example-S11} &
    \includegraphics[height=47.5mm]{figures/08-example-S22}
\end{tabular}

\paragraph{Homework ideas}

\begin{itemize}\noitemsep
    \item Detail extraction of second-order statistics from digitized images.
\end{itemize}

\Reference

\Acknowledgement 

\end{document}
 