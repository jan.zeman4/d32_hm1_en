\documentclass{MHM1_lectures}

\newcommand{\vek}[1]{{\boldsymbol #1}}
\newcommand{\phs}[1]{^{(#1)}}
\newcommand{\fl}{'} % Fluctuation
\newcommand{\de}{d}
\newcommand{\ft}[1]{\widehat{#1}}
\newcommand{\imu}{\mathrm{i}\,}
\newcommand{\trn}{^T}

\begin{document}
    
\maketitle{5}{Advanced theory of effective properties~I: Preliminaries to Eshelby inclusion problem}

\tableofcontents

\section{Fourier transform}

\begin{itemize}\noitemsep

    \item Euler formula
    %
    \begin{align*}
        \exp( \imu \vek{\xi} \trn \vek{x} )
        =
        \cos( \vek{\xi} \trn \vek{x} )
        +
        \imu 
        \sin( \vek{\xi} \trn \vek{x} )
    \end{align*}
    %
    provides $\vek{\xi}$- and $\vek{x}$-parameterized basis on $\mathbb{R}^d$.  

    \item \emph{Forward} Fourier transform of $f : \mathbb{R}^d \rightarrow \mathbb{R}$
    %
    \begin{align}\label{eq:FT}
        \ft{f}( \vek{\xi} )
        =
        \int_{\mathbb{R}^d}
        f( \vek{x} )
        \exp( \imu \vek{\xi} \trn \vek{x} )
        \de \vek{x}
        \in \mathbb{C}
        \text{ for }
        \vek{\xi} \in \mathbb{R}^d
    \end{align}
    %
    (Fourier coefficient associated with frequency $\vek{\xi}$ or function value in the Fourier space)

    \item \emph{Inverse} Fourier transform of $\ft{f} : \mathbb{R}^d \rightarrow \mathbb{C}$
    %
    \begin{align}\label{eq:iFT}
        f( \vek{x} )
        =
        \frac{1}{(2 \pi)^d}
        \int_{\mathbb{R}^d}
        \ft{f}( \vek{\xi} )
        \exp( -\imu \vek{\xi} \trn \vek{x} )
        \de \vek{\xi}
        \in \mathbb{R}
        \text{ for }
        \vek{x} \in \mathbb{R}^d
    \end{align}
    %
    (function value in the real space).

    \item $f \longleftrightarrow \ft{f}$

    \item Some operations are difficult in the real space, but easy in the real space. Example: zero-mean property
    %
    \begin{align*}
        \ft{f}( \vek{0} )
        =
        \int_{\mathbb{R}^d}
        f( \vek{x} )
        \exp( \imu \vek{0} \trn \vek{x} )
        \de \vek{x}
        =
        \int_{\mathbb{R}^d}
        f( \vek{x} )
        \de \vek{x}
        =
        0
    \end{align*}
    %
    More will be introduced when needed.

    \item Introduced by Fourier~\cite{Fourier:1878}
\end{itemize}

\section{Green's function}

\begin{itemize}\noitemsep
    \item Generalization of influence lines of structural mechanics
    
    \item Special solution of field equation $G\phs{0} : \mathbb{R}^d \rightarrow \mathbb{R}$ for 
    %
    \begin{itemize}\noitemsep
        \item Constant material data $\vek{L}\phs{0} \succ \vek{0}$
        \item Point source of intensity one located at $\vek{0}$
    \end{itemize}
    (other applications, e.g., in boundary element methods)

    \item Governing equations
    %
    \begin{align*}
        - \vek{\nabla}\trn 
        \vek{L}\phs{0}
        \vek{\nabla} G\phs{0}( \vek{x} )
        & =
        \delta( \vek{x} )
        \text{ for }
        \vek{x} \in \mathbb{R}^d
        \\
        \int_{\mathbb{R}^d} G\phs{0}( \vek{x} ) \de\vek{x} 
        & =
        0
    \end{align*}
    %
    where $\delta( \vek{0} )$ denotes the Dirac generalized function~(distribution), implicitly defined as
    %
    \begin{align}\label{eq:dirac_def}
        \int_{\mathbb{R}^d}
        \delta( \vek{x} )
        \varphi( \vek{x} )
        \de \vek{x}
        =
        \varphi( \vek{0} )
        \text{ for any }
        \varphi \in C^\infty( \mathbb{R}^d )
    \end{align}

    \item From~\eqref{eq:iFT} transform the left-hand side into
    %
    \begin{align*}
        - \vek{\nabla}\trn 
        \vek{L}\phs{0}
        \vek{\nabla} 
        G\phs{0}( \vek{x} )
        = 
        \frac{1}{(2 \pi)^d}
        -
        \vek{\nabla}_{\vek x}\trn 
        \vek{L}\phs{0}
        \vek{\nabla}_{\vek x} 
        \int_{\mathbb{R}^d}
        \ft{G}\phs{0}( \vek{\xi} )
        \exp( -\imu \vek{\xi} \trn \vek{x} )
        \de \vek{\xi}
        \\
        -\vek{\nabla}_{\vek x}\trn 
        \vek{L}\phs{0}
        \vek{\nabla}_{\vek x} 
        \exp( -\imu \vek{\xi} \trn \vek{x} )
        =
        \imu 
        \vek{\nabla}_{\vek x}\trn 
        \vek{L}\phs{0}
        \vek{\xi} 
        \exp( -\imu \vek{\xi} \trn \vek{x} )
        =
        -(\imu)^2
        \vek{\xi}\trn 
        \vek{L}\phs{0}
        \vek{\xi} 
        \exp( -\imu \vek{\xi} \trn \vek{x} )
        \\
        - \vek{\nabla}\trn 
        \vek{L}\phs{0}
        \vek{\nabla} 
        G\phs{0}( \vek{x} )
        =
        \frac{1}{(2 \pi)^d}
        \int_{\mathbb{R}^d}
        \vek{\xi}\trn 
        \vek{L}\phs{0}
        \vek{\xi}
        \cdot 
        \ft{G}\phs{0}( \vek{\xi} )
        \exp( -\imu \vek{\xi} \trn \vek{x} )
        \de \vek{\xi}  
    \end{align*}
    (real-space derivatives $\rightarrow$ multiplication in the Fourier space)

    \item Fourier transform~\eqref{eq:FT} or the right-hand side gives
    %
    \begin{align*}
        \ft{\delta}( \vek{\xi} )
        =
        \int_{\mathbb{R}^d}
        \delta( \vek{x} )
        \exp( \imu \vek{\xi} \trn \vek{x} )
        \de \vek{x}
        \stackrel{\eqref{eq:dirac_def}}{=}
        \exp( \imu \vek{\xi} \trn \vek{0} )
        =
        1
        \text{ for any } \vek{\xi} \in \mathbb{R}^d
    \end{align*}
    %
    and the inverse Fourier transform reveals that
    %
    \begin{align}\label{eq:Diract_Fourier}
        \delta( \vek{x} )
        =
        \frac{1}{(2 \pi)^d}
        \int_{\mathbb{R}^d}
        1 \cdot
        \exp( -\imu \vek{\xi} \trn \vek{x} )
        \de \vek{\xi}
    \end{align}

    \item Collecting both sides together
    %
    \begin{align*}
        \frac{1}{(2 \pi)^d}
        \int_{\mathbb{R}^d}
        \vek{\xi}\trn 
        \vek{L}\phs{0}
        \vek{\xi}
        \cdot 
        \ft{G}\phs{0}( \vek{\xi} )
        \exp( -\imu \vek{\xi} \trn \vek{x} )
        \de \vek{\xi} 
        =
        \frac{1}{(2 \pi)^d}
        \int_{\mathbb{R}^d}
        1 \cdot
        \exp( -\imu \vek{\xi} \trn \vek{x} )
        \de \vek{\xi}
        \text{ for }
        \vek{x} \in \mathbb{R}^d
    \end{align*}

    \item Therefore,
    %
    \begin{align*}
        \vek{\xi}\trn 
        \vek{L}\phs{0}
        \vek{\xi}
        \cdot 
        \ft{G}\phs{0}( \vek{\xi} )
        =
        1
        \text{ for almost all }
        \vek{\xi} \in \mathbb{R}^d
    \end{align*}
    %
    and
    %
    \begin{align*}
        \ft{G}\phs{0}( \vek{\xi} )
        & =
        \begin{cases}
            \left( 
                \vek{\xi}\trn 
                \vek{L}\phs{0}
                \vek{\xi} 
            \right)^{-1} & \text{for } \vek{\xi} \neq \vek{0}
            \\
            0 & \text{for } \vek{\xi} = \vek{0}
        \end{cases}
        \\
        G\phs{0}( \vek{x} )
        & =
        \frac{1}{(2 \pi)^d}
        \int_{\mathbb{R}^d \backslash \{ \vek{0}\} }
        \left( 
            \vek{\xi}\trn 
            \vek{L}\phs{0}
            \vek{\xi} 
        \right)^{-1}
        \exp( -\imu \vek{\xi} \trn \vek{x} )
        \de \vek{\xi}
    \end{align*}

    \item Spherical coordinates~($d=3$)
    %
    \begin{align*}
        \vek{\xi} = r \vek{\omega} \text{ for } r = \| \vek{\xi} \| \in [0, \infty ), \vek{\omega} \in \partial S^3, 
        &&  
        \de\vek{\xi} = r^2 \de{r} \de\vek{\omega}
    \end{align*}

    \item After transformation
    %
    \begin{align*}
        G\phs{0}( \vek{x} )
        =
        \frac{1}{8 \pi^3}
        \int_{S^2}
        \int_{0}^{\infty}
        r^{-2}
        \left( 
            \vek{\omega}\trn 
            \vek{L}\phs{0}
            \vek{\omega} 
        \right)^{-1}
        \exp( -\imu r \vek{\omega} \trn \vek{x} )
        r^2 \de{r} \de\vek{\omega}
    \end{align*}

    \item From the definition of the Dirac delta distribution~\eqref{eq:Diract_Fourier} for $d=1$, we have
    \begin{align}\label{eq:Dirac_1D}
    2
    \int_{0}^{\infty}
    \exp( -\imu r \vek{\omega} \trn \vek{x} )
    \de r
    =
    \int_{-\infty}^{\infty}
    \exp( -\imu r \vek{\omega} \trn \vek{x} )
    \de r
    =
    2\pi \delta( \vek{\omega}\trn \vek{x} )
    \end{align}

    \item Green's function representation
    %
    \begin{align*}
        G\phs{0}( \vek{x} )
        =
        \frac{1}{8 \pi^2}
        \int_{\partial S^3}
        \left( 
            \vek{\omega}\trn 
            \vek{L}\phs{0}
            \vek{\omega} 
        \right)^{-1}
        \delta( \vek{\omega}\trn \vek{x} )
        \de\vek{\omega}
    \end{align*}
    
    \item Derivation after Laws~\cite{Laws:1977:DSS} and Willis~\cite{Willis1977}.
\end{itemize}

\section{Eshelby inclusion problem}

\begin{itemize}\noitemsep

    \item Uniform polarization flux $\vek{p} \in \mathbb{R}^d$ introduced inside $\Omega_\mathrm{I}$ 
    
    \item Governing equations
    %
    \begin{align*}
        \vek{\nabla}\trn 
        \vek{L}\phs{0}
        \left( 
            -
            \vek{\nabla} u_\mathrm{I}( \vek{x} )
            +
            \chi_{\Omega_\mathrm{I}}( \vek{x} ) \vek{p}
        \right)
        & = 
        0
        \\
        - \vek{\nabla}\trn 
        \vek{L}\phs{0}
        \vek{\nabla} u_\mathrm{I}( \vek{x} )
        & =
        - \vek{\nabla}\trn 
        [ \chi_{\Omega_\mathrm{I}}( \vek{x} ) 
        \vek{L}\phs{0}
    \end{align*}

    \item We are after relating the gradient field \emph{inside} the inclusion
    %
    \begin{align*}
        \vek{e}_\mathrm{I}( \vek{x} ) = - \vek{\nabla} u_\mathrm{I} ( \vek{x} )
        \text{ for }
        \vek{x} \in \Omega_\mathrm{I}
    \end{align*}
    %
    to the polarization vector $\vek{p}$~(the so-called interior Eshelby problem).
    
    \item Because of homogeneous $\vek{L}\phs{0}$, we can adopt the Green's function
    
    \item For general source $Q( \vek{x} ) : \mathbb{R}^d \rightarrow \mathbb{R}$
    %
    \begin{align*}
        u( \vek{x} )
        =
        \int_{\mathbb{R}^d}
        G\phs{0}( \vek{x} - \vek{y} ) \de\vek{y}
    \end{align*}

    \item For the inclusion problem
    %
    \begin{align*}
        u_\mathrm{I}( \vek{x} ) 
        & = 
        -
        \int_{\mathbb{R}^d}
        G\phs{0}( \vek{x} - \vek{y} )
        \vek{\nabla}_{\vek y}\trn 
        \chi_{\Omega_\mathrm{I}}( \vek{x} ) 
        \vek{L}\phs{0}
        \vek{p}
        \de\vek{y}
        \\
        \vek{e}_\mathrm{I}( \vek{x})
        & =
        \vek{\nabla}_{\vek x}
        \int_{\mathbb{R}^d}
        G\phs{0}( \vek{x} - \vek{y} )
        \vek{\nabla}_{\vek y}\trn 
        \chi_{\Omega_\mathrm{I}}( \vek{x} ) 
        \vek{L}\phs{0}
        \vek{p}
        \de\vek{y}
        = 
        \vek{?}
        \text{ for }
        \vek{x} \in \Omega_\mathrm{I}
    \end{align*}

    \item First introduced and solved by Eshelby~\cite{Eshelby:1957:DEF} for linear elasticity

\end{itemize}

\paragraph{Homework ideas}

\begin{itemize}\noitemsep
    \item Derive the explicit expression of the Green function for isotropic $\vek{L}\phs{0}$
\end{itemize}

\Reference

\Acknowledgement 

\end{document}
