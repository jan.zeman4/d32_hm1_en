\documentclass{MHM1_lectures}

\newcommand{\vek}[1]{{\boldsymbol #1}}
\newcommand{\phs}[1]{^{(#1)}}
\newcommand{\fl}{'} % Fluctuation
\newcommand{\de}{d}

\begin{document}
    
\maketitle{4}{Elementary theory of effective properties: Reuss and Voigt estimates and bounds, laminates}

\tableofcontents

\section{Voigt and Reuss estimates}

\begin{itemize}
\item Recall from Lecture~III that
%
\begin{align*}
    \vek{L}^\mathrm{eff} = 
    \left\langle      
    \vek{L}( \vek{x})
    \left( \vek{E} + \vek{A}\fl( \vek{x} \right)   
    \right\rangle,
    &&
    \vek{M}^\mathrm{eff}
    =
    \left\langle
    \vek{M}( \vek{x})
    \left( \vek{I} + \vek{B}\fl(\vek{x})  \right)
    \right\rangle
\end{align*}

\item Take $\vek{A}\fl = \vek{B}\fl \equiv \vek{0}$~(no fluctuations because of heterogeneity)

\item Voigt~\cite{Voigt:1887:TSE} and Reuss~\cite{Reuss:1929:BFM} \emph{estimates}
%
\begin{align*}
    \vek{L}^{\mathrm V} = 
    \left\langle      
    \vek{L}( \vek{x})
    \right\rangle,
    &&
    \vek{M}^{\mathrm R}
    =
    \left\langle
    \vek{M}( \vek{x})
    \right\rangle
\end{align*}

\item Multi-phase composites~(recall Lecture~I)
%
\begin{align*}
    \vek{L}^{\mathrm V} = 
    \left\langle      
    \vek{L}( \vek{x})
    \right\rangle
    & =
    \left\langle 
    \sum_{r=1}^{N}     
    \chi\phs{r}( \vek{x})
    \vek{L}\phs{r}
    \right\rangle 
    =
    \sum_{r=1}^{N}
    \left\langle 
    \chi\phs{r}( \vek{x})
    \right\rangle 
    \vek{L}\phs{r}
    =
    \sum_{r=1}^N
    \left( 
    \frac{1}{|\Omega|}
    \int_{\Omega}
    \chi\phs{r}( \vek{x} )
    \de \vek{x}
    \right)
    \vek{L}\phs{r}
    \\
    & =
    \sum_{r=1}^N 
    \frac{|\Omega\phs{r}|}{|\Omega|}
    \vek{L}\phs{r}
    =
    \sum_{r=1}^N 
    c\phs{r} \vek{L}\phs{r}
\end{align*}
by analogy
%
\begin{align*}
\vek{M}^{\mathrm R}
=
\sum_{r=1}^N 
c\phs{r} \vek{M}\phs{r}
\end{align*}

\item For isotropic phases, $\vek{L}^\mathrm{V}$ and $\vek{M}^\mathrm{R}$ are also isotropic
%
\begin{align*}
    \vek{L}^\mathrm{V} = 
    \left( 
    \sum_{r=1}^N 
    c\phs{r} L\phs{r} 
    \right)
    \vek{I}
    =
    L^\mathrm{V} \vek{I}, 
    &&
    \vek{M}^\mathrm{R} = 
    \left( 
    \sum_{r=1}^N 
    c\phs{r} M\phs{r} 
    \right)
    \vek{I}
    =
    M^\mathrm{R} \vek{I},
\end{align*}

\item Observe that
%
\begin{align}\label{eq:duality_gap}
\vek{L}^{\mathrm V} 
\neq 
\left( 
    \vek{M}^{\mathrm R}
\right)^{-1}
\end{align}

\end{itemize}

\section{Voigt and Reuss bounds}

\begin{itemize}
\item Recall from Lecture~III that
%
\begin{align*}
    \vek{E}^T \vek{L}^\mathrm{eff} \vek{E}
    =
    \min_{\vek{v}\fl \in \mathcal{E}\fl} 
    \left\langle
    \left( \vek{E} + \vek{v}\fl ( \vek{x}) \right)^T
    \vek{L}( \vek{x} )
    \left( \vek{E} + \vek{v}\fl ( \vek{x}) \right)    
    \right\rangle
    \stackrel{\vek{v}\fl = \vek{0}}{\leq}
    \left\langle
    \vek{E}^T
    \vek{L}( \vek{x} )
    \vek{E}    
    \right\rangle
    =
    \vek{E}^T
    \left\langle
    \vek{L}( \vek{x} )  
    \right\rangle
    \vek{E}         
\end{align*}
%
for arbitrary $\vek{E} \in \mathbb{R}^d$.

\item Hence
%
\begin{align*}
    \vek{L}^\mathrm{eff}
    \preceq 
    \left\langle
    \vek{L}( \vek{x} )  
    \right\rangle
    =
    \vek{L}^{\mathrm V}
\end{align*}
%
and, by analogy,
%
\begin{align*}
    \vek{M}^\mathrm{eff}
    \preceq 
    \left\langle
    \vek{M}( \vek{x} )  
    \right\rangle
    =
    \vek{M}^{\mathrm R}
\end{align*}

\item By the properties of the L\"{o}wner partial order and by the inverse property
%
\begin{align}\label{eq:Voigt_Reuss_bounds}
    \left( \vek{M}^\mathrm{R}\right)^{-1}
    =
    \left( 
        \left\langle
        \vek{L}^{-1}( \vek{x} )  
        \right\rangle    
    \right)^{-1}
    \preceq
    \left( \vek{M}^\mathrm{eff} \right)^{-1}
    =
    \vek{L}^\mathrm{eff}
    \preceq 
    \left\langle
    \vek{L}( \vek{x} )  
    \right\rangle
    =
    \vek{L}^{\mathrm V}
\end{align}

\item Hence, $\left( \vek{M}^\mathrm{R}\right)^{-1}$ and $\vek{L}^{\mathrm V}$ provide lower and upper bounds on the effective properties~(in the sense of the L\"{o}wner partial order, the quadratic forms, in the energetic sense) 

\item First proven by Hill~\cite{Hill:1963:EPS} for elasticity

\end{itemize}

\section{Laminates}

\begin{itemize}
    \item For simplicity, consider two-dimensional laminate~($d=2$) occupying $\mathbb{R}^2$ and coefficients $\vek{L}(x_1)$
        
    \item Potential $u\fl( x_1, x_2 ) = u\fl( x_1 )$~(with a slight abuse of notation)

    \item Intensity decomposition
    %
    \begin{align*}
        \begin{bmatrix}
            e_1( x_1, x_2 ) \\
            e_2( x_1, x_2 )
        \end{bmatrix}
        =
        \begin{bmatrix}
            E_1 \\ E_2
        \end{bmatrix}
        -
        \begin{bmatrix}
            \partial / \partial x_1 \\
            \partial / \partial x_2
        \end{bmatrix}
        %
        u\fl( x_1 )
        =
        \begin{bmatrix}
            E_1 + e\fl_1( x_1 ) \\ E_2
        \end{bmatrix}
    \end{align*}

    \item Flux decomposition
    %
    \begin{align*}
        \begin{bmatrix}
            \jmath_1( x_1, x_2 ) \\
            \jmath_2( x_1, x_2 )
        \end{bmatrix}
        =
        \begin{bmatrix}
            J_1 \\ J_2 
        \end{bmatrix}
        +
        \begin{bmatrix}
            \jmath\fl_1( x_1 ) \\
            \jmath\fl_2( x_1 )
        \end{bmatrix}
    \end{align*}
    %
    and, by conservation,
    %
    \begin{align*}
        \begin{bmatrix}
            \partial / \partial x_1 & 
            \partial / \partial x_2 
        \end{bmatrix}
        \begin{bmatrix}
            \jmath\fl_1( x_1 ) \\
            \jmath\fl_2( x_1 )
        \end{bmatrix}
        =
        0
        & \Longrightarrow
        \frac{\de}{\de x_1}
        \jmath\fl_1( x_1 )
        = 0
    \end{align*}
    %
    Therefore, $\jmath\fl_1( x_1 ) = 0$~(zero-mean property) and we obtain
    %
    \begin{align*}
        \begin{bmatrix}
            \jmath_1( x_1, x_2 ) \\
            \jmath_2( x_1, x_2 )
        \end{bmatrix}
        =
        \begin{bmatrix}
            J_1 \\ J_2 + \jmath\fl_2( x_1 )
        \end{bmatrix}
    \end{align*}

    \item Local constitutive properties
    %
    \begin{subequations}
    \begin{align}
        J_1 
        & = 
        L_{11}( x_1 ) e_1(x_1) + L_{12}( x_1 ) E_2 
        \label{eq:lam_1} \\
        \jmath_2( x_1 ) 
        & = 
        L_{12}( x_1 ) e_1(x_1) + L_{22}( x_1 ) E_2
        \label{eq:lam_2} 
    \end{align}
    \end{subequations}
    %
    to which we can apply the static condensation~(Schur complement) to express $x_1$-dependent components in terms of constant components

    \item From~\eqref{eq:lam_1}, we have
    %
    \begin{align*}
        e_1(x_1)
        =
        L^{-1}_{11}( x_1 ) 
        \left( J_1 - L_{12}( x_1 ) E_2 \right)
    \end{align*}
    %
    and substitute it to~\eqref{eq:lam_2}
    %
    \begin{align*}
        \jmath_2( x_1 ) 
        & = 
        L_{12}( x_1 ) \left( 
            L^{-1}_{11}( x_1 ) 
            \left( J_1 - L_{12}( x_1 ) E_2 \right)
        \right) 
        + L_{22}( x_1 ) E_2
    \end{align*}

    \item Therefore
    %
    \begin{align*}
        \begin{bmatrix}
            e_1(x_1) \\ \jmath_2( x_1 )
        \end{bmatrix}
        =
        \begin{bmatrix}
            L^{-1}_{11}( x_1 ) & 
            - L_{12} L^{-1}_{11}( x_1 ) \\
            L_{12} L^{-1}_{11}( x_1 ) & 
            L_{22}( x_1 ) - L^2_{12}L^{-1}_{11}( x_1 )
        \end{bmatrix}
        %
        \begin{bmatrix}
            J_1 \\ E_2
        \end{bmatrix}
    \end{align*}
    
    \item Average left and right sides
    %
    \begin{align*}
        \begin{bmatrix}
            E_1 \\ J_2
        \end{bmatrix}
        =
        \begin{bmatrix}
            \left\langle 
            L^{-1}_{11}( x_1 ) 
            \right\rangle
            & 
            -
            \left\langle 
            L_{12} L^{-1}_{11}( x_1 )
            \right\rangle
            \\
            \left\langle 
            L_{12} L^{-1}_{11}( x_1 )
            \right\rangle
            & 
            \left\langle 
            L_{22}( x_1 ) 
            \right\rangle
            -
            \left\langle 
            L^2_{12} L^{-1}_{11}( x_1 )
            \right\rangle
        \end{bmatrix}
        \begin{bmatrix}
            J_1 \\ E_2
        \end{bmatrix}
    \end{align*}

    \item Now we recall that $E_1$ and $E_2$ is given and $J_1$ and $J_2$ are to be determined
    %
    \begin{align*}
        E_1 
        & =
        \left\langle 
        L^{-1}_{11}( x_1 ) 
        \right\rangle
        J_1
        -
        \left\langle 
        L_{12} L^{-1}_{11}( x_1 )
        \right\rangle
        E_2
        \Longrightarrow \\
        J_1 
        & =
        \left\langle 
        L^{-1}_{11}( x_1 ) 
        \right\rangle^{-1}
        E_1
        +
        \left\langle 
        L^{-1}_{11}( x_1 ) 
        \right\rangle^{-1}
        \left\langle 
        L_{12} L^{-1}_{11}( x_1 )
        \right\rangle
        E_2
    \end{align*}
    %
    and 
    %
    \begin{align*}
        J_2 
        & =
        \left\langle 
        L^{-1}_{11}( x_1 ) 
        \right\rangle^{-1}
        \left\langle 
        L_{12} L^{-1}_{11}( x_1 )
        \right\rangle
        \left(
        E_1
        +
        \left\langle 
        L_{12} L^{-1}_{11}( x_1 )
        \right\rangle
        E_2
        \right)
        \\
        & +
        \left( 
        \left\langle 
        L_{22}( x_1 ) 
        \right\rangle
        -
        \left\langle 
        L^2_{12} L^{-1}_{11}( x_1 )
        \right\rangle
        \right)
        E_2
    \end{align*}

    \item Finally
    %
    \begin{align*}
        %
        \hspace*{-4ex}
        %
        \vek{L}^\mathrm{eff}
        =
        \begin{bmatrix}
            \left\langle 
            L^{-1}_{11}( x_1 ) 
            \right\rangle^{-1}
            &    
            \left\langle 
            L^{-1}_{11}( x_1 ) 
            \right\rangle^{-1}
            \left\langle 
            L_{12}( x_1 ) L^{-1}_{11}( x_1 )
            \right\rangle
            \\
            \left\langle 
            L^{-1}_{11}( x_1 ) 
            \right\rangle^{-1}
            \left\langle 
            L_{12}( x_1 ) L^{-1}_{11}( x_1 )
            \right\rangle
            &          
            \left\langle 
            L^{-1}_{11}( x_1 ) 
            \right\rangle^{-1}
            \left\langle 
            L_{12}( x_1 ) L^{-1}_{11}( x_1 )
            \right\rangle^2
            -
            \left\langle 
            L^2_{12} L^{-1}_{11}( x_1 )
            \right\rangle
            +
            \left\langle 
            L_{22}( x_1 ) 
            \right\rangle    
        \end{bmatrix}
    \end{align*}

    \item Orthotropic $N$-phase laminate
    %
    \begin{align*}
        \vek{L}^\mathrm{eff}
        =
        \begin{bmatrix}
            \displaystyle
            \left( 
                \sum_{r=1}^N
                c\phs{r}  
                \bigl( L_{11}\phs{r} \bigr)^{-1}
            \right)^{-1} & 0 
            \\
            0 
            & \displaystyle 
            \sum_{r=1}^N c\phs{r} L_{22}\phs{r}
        \end{bmatrix}
        =
        \begin{bmatrix}
           \left(  M_{11}^\mathrm{R} \right)^{-1} & 0 \\
           0 & L_{22}^\mathrm{V}
        \end{bmatrix}
    \end{align*}
    %
    possesses orthotropic effective properties.

    \item Isotropic $N$-phase laminate
    %
    \begin{align*}
        \vek{L}^\mathrm{eff}
        =
        \begin{bmatrix}
            \displaystyle
            \left( 
                \sum_{r=1}^N
                c\phs{r}  
                \bigl( L\phs{r} \bigr)^{-1}
            \right)^{-1} & 0 
            \\
            0 
            & \displaystyle 
            \sum_{r=1}^N c\phs{r} L\phs{r}
        \end{bmatrix}
        =
        \begin{bmatrix}
           \left(  M^\mathrm{R} \right)^{-1} & 0 \\
           0 & L^\mathrm{V}
        \end{bmatrix}
    \end{align*}
    %
    possesses orthotropic effective properties.

    \item General formula due to Backus~\cite{Backus:1962:LWE}
\end{itemize}

\paragraph{Homework ideas}

\begin{itemize}\noitemsep
    \item Explain~\eqref{eq:duality_gap}
    \item Generalize the lamination results to $d=3$
    \item Prove that the lamination formulae satisfy~\eqref{eq:Voigt_Reuss_bounds}
\end{itemize}

\Reference

\Acknowledgement 

\end{document}
