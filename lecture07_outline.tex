\documentclass{MHM1_lectures}

\newcommand{\vek}[1]{{\boldsymbol #1}}
\newcommand{\phs}[1]{^{(#1)}}
\newcommand{\fl}{'} % Fluctuation
\newcommand{\de}{d}
\newcommand{\ft}[1]{\widehat{#1}}
\newcommand{\imu}{\mathrm{i}\,}
\newcommand{\trn}{^T}

\begin{document}
    
\maketitle{7}{Advanced theory of effective properties~III}

\tableofcontents

\section{Concentration factors}

\begin{itemize}\noitemsep

    \item Recall the general expressions effective media
    %
    \begin{align*}
        \vek{L}^\mathrm{eff}
        =
        \sum_{r=1}^N
        c\phs{r} \vek{L}\phs{r} \vek{A}\phs{r},
        &&
        \sum_{r=1}^N c\phs{r} \vek{A}\phs{r}
        =
        \vek{I}
    \end{align*}

    \item Matrix~($r=1$)--inclusion~($r=2, \ldots, N$) composites: We will estimate $\vek{A}\phs{r}$ based on the Eshelby solution~(accurate approximation) for $r > 1$
    
    \item Alternative expressions 
    \begin{align*}
        \vek{L}^\mathrm{eff}
        & =
        c\phs{1}\vek{L}\phs{1} \vek{A}\phs{1}
        +
        \sum_{r=2}^N
        c\phs{r} \vek{L}\phs{r}
        \vek{A}\phs{r}
        =
        \vek{L}\phs{1} \left( 
            \vek{I} - \sum_{r=2}^N c\phs{r} \vek{A}\phs{r}
        \right)
        +
        \sum_{r=2}^N
        c\phs{r} \vek{L}\phs{r}
        \vek{A}\phs{r}
        \\
        & =
        \vek{L}\phs{1}
        +
        \sum_{r=2}^N
        c\phs{r} \left( \vek{L}\phs{r} - \vek{L}\phs{1} \right)
        \vek{A}\phs{r}
    \end{align*}

    \item Filter test: For any $c\phs{t} = 1, \ldots, N$ 
    %
    \begin{align}\label{eq:consistency_filter}
        \vek{L}^\mathrm{eff}
        =
        \vek{L}\phs{t}
    \end{align}
    
    \item For the previous paramaterization, automatically met for $t=1$.

\end{itemize}

\section{Dilute approximation}

\begin{itemize}\noitemsep

\item Superimpose the Eshelby solution for $\vek{L}\phs{0} = \vek{L}\phs{1}$ and $\vek{L}_\mathrm{I}  = \vek{L}\phs{r} \rightarrow \vek{A}\phs{r} = \vek{A}_\mathrm{dil}\phs{r}$
%
\begin{align*}
    \vek{A}\phs{r}_\mathrm{dil}
    =
    \left( 
        \vek{I}
        +
        \vek{P}\phs{1, r} \left( \vek{L}\phs{r} - \vek{L}\phs{1} \right)
    \right)^{-1}
\end{align*}

\item Neglects interaction \emph{among} inclusions, but $\vek{A}\phs{r} \neq \vek{I}$~(Voigt)

\item Final formula
%
\begin{align*}
    \vek{L}^\mathrm{eff}_\mathrm{dil}
    =
    \vek{L}\phs{1}
    +
    \sum_{r=2}^N
    c\phs{r} \left( \vek{L}\phs{r} - \vek{L}\phs{1} \right)
    \left( 
        \vek{I}
        +
        \vek{P}\phs{1, r} \left( \vek{L}\phs{r} - \vek{L}\phs{1} \right)
    \right)^{-1}    
\end{align*}

\item Fails to satisfy the filter test~\eqref{eq:consistency_filter} for $t = 2, \ldots, N$.

\item{\bf Example} Isotropic phases, spherical inclusions: $\vek{L}^\mathrm{eff}_\mathrm{dil} = L^\mathrm{eff}_\mathrm{dil} \vek{I}$
%
\begin{align*}
    A\phs{r}_\mathrm{dil}
    & = 
    \frac{3 L\phs{1}}{2 L\phs{1} + L\phs{r}} 
    \\
    L^\mathrm{eff}_\mathrm{dil} 
    & =
    L\phs{1}
    +
    \sum_{r=2}^N
    c\phs{r}
    \left( L\phs{r} - L\phs{1} \right)
    \frac{3 L\phs{1}}{2 L\phs{1} + L\phs{r}} 
    \\
    \frac{%
    L^\mathrm{eff}_\mathrm{dil}}{%
    L\phs{1}} 
    & = 
    1 + \sum_{r=2}^N 
    3 c\phs{r}
    \frac{L\phs{r} - L\phs{1}}{L\phs{r} + 2 L\phs{1}}
\end{align*}

\end{itemize}

\section{Self-consistent method}

\begin{itemize}

    \item Embed each inclusion into $\vek{L}^\mathrm{eff}$
    %    
    \begin{align*}
        \vek{A}\phs{r}_\mathrm{sc}
        =
        \left( 
            \vek{I}
            +
            \vek{P}\phs{\mathrm{eff}, r} \left( \vek{L}\phs{r} - \vek{L}^\mathrm{eff}_\mathrm{sc} \right)
        \right)^{-1}
    \end{align*}

    \item Estimate of effective coefficients
    %
    \begin{align*}
        \vek{L}^\mathrm{eff}_\mathrm{sc}
        =
        \vek{L}\phs{1}
        +
        \sum_{r=2}^N
        c\phs{r} \left( \vek{L}\phs{r} - \vek{L}\phs{1} \right)
        \left( 
            \vek{I}
            +
            \vek{P}\phs{\mathrm{eff}, r} \left( \vek{L}\phs{r} - \vek{L}^\mathrm{eff}_\mathrm{sc} \right)
        \right)^{-1}
    \end{align*}
    
    \item System on non-linear equations for the unknown $\vek{L}^\mathrm{eff}_\mathrm{sc} \rightarrow$ \emph{implicit} method
    
    \item Filter test~\eqref{eq:consistency_filter} for $t = 2, \ldots, N$:
    %
    \begin{align*}
        \vek{L}^\mathrm{eff}_\mathrm{sc}
        =
        \vek{L}\phs{1}
        +
        \left( \vek{L}\phs{t} - \vek{L}\phs{1} \right)
        \left( 
            \vek{I}
            +
            \vek{P}\phs{\mathrm{eff}, t} \left( \vek{L}\phs{t} - \vek{L}^\mathrm{eff}_\mathrm{sc} \right)
        \right)^{-1}
    \end{align*}
    %
    $\vek{L}^\mathrm{eff}_\mathrm{sc} = \vek{L}\phs{t}$ the system.~$\checkmark$

    \item{\bf Example} Two-phase composite, isotropic matrix and spherical inclusions: $\vek{L}^\mathrm{eff}_\mathrm{sc} = L^\mathrm{eff}_\mathrm{sc} \vek{I}$
\end{itemize}
%
\begin{align*}
        L^\mathrm{eff}_\mathrm{sc}
        & =
        L\phs{1}
        +
        c\phs{2}
        \left( L\phs{2} - L\phs{1} \right)
        \frac{3 L^\mathrm{eff}_{sc}}{2 L^\mathrm{eff}_{sc} + L\phs{2}}
        \\
        \left( 
            L^\mathrm{eff}_\mathrm{sc}
            -
            L\phs{1}    
        \right)
        \left( 
            2 L^\mathrm{eff}_{sc} + L\phs{2}
        \right)
        & = 
        3c\phs{2}
        \left( L\phs{2} - L\phs{1} \right) L^\mathrm{eff}_{sc}
        \\
        \underbrace{2}_{a}
        \left( L^\mathrm{eff}_\mathrm{sc} \right)^2
        +
        \underbrace{\left(
            L\phs{2}
            -
            2 L\phs{1} 
            -
            3c\phs{2}
            \left( L\phs{2} - L\phs{1} \right)
        \right)}_{b}
        L^\mathrm{eff}_\mathrm{sc}
        \underbrace{- 
        L\phs{1} L\phs{2}}_{c}
        & = 0 
\end{align*}
%
\begin{itemize}
    \item Hence
    %
    \begin{align*}
        b = 
        L\phs{1} \left( 3c\phs{2} - 2 \right)
        +
        L\phs{2} \left( 1 - 3 c\phs{2} \right)
        =  
        L\phs{1} \left( 1 - 3 c\phs{1} \right)
        +
        L\phs{2} \left( 1 - 3 c\phs{2} \right)
    \end{align*}
    %
    and
    %
    \begin{align*}
        L^\mathrm{eff}_\mathrm{sc}
        =
        \frac{1}{4}
        \left(
            L\phs{1} \left( 3 c\phs{1} - 1 \right)
            +
            L\phs{2} \left( 3 c\phs{2} - 1 \right)    
        +
        \sqrt{%
            \left(
                L\phs{1} \left( 3 c\phs{1} - 1 \right)
                +
                L\phs{2} \left( 3 c\phs{2} - 1 \right)    
            \right)^2
            +
            8 L\phs{1} L\phs{2}
        }
        \right)
    \end{align*}

    \item Phase-interchange property $1 \leftrightarrow 2$
    
    \item Initially developed by Bruggemann~\cite{Bruggeman1935} extended to polycrystals by Hershey~\cite{Hershey1954} and composites by Hill~\cite{Hill1965} and Kr\"{o}ner~\cite{Kroner1958}.
    
\end{itemize}

\section{Mori-Tanaka-Benveniste method}

\begin{itemize}
    \item Constant intensity fields in inclusions embedded in the matrix phase
    %
    \begin{align*}
        \vek{e}\phs{1}
        & =  
        \vek{A}\phs{1}_\mathrm{M-T}
        \vek{E},
        \\
        \vek{e}\phs{r} 
        & =
        \vek{A}\phs{r}_\mathrm{dil}
        \vek{e}\phs{1}
        = 
        \vek{A}\phs{r}_\mathrm{dil}
        \vek{A}\phs{1}_\mathrm{M-T}        
        \vek{E}
        =    
        \vek{A}\phs{r}_\mathrm{M-T}
        \vek{E},
        &&
        r = 2, \ldots, N
    \end{align*} 

    \item Consistency condition
    %
    \begin{align*}
        \vek{I}
        & =
        \sum_{r=1}^{N}
        c\phs{r} \vek{A}\phs{r}_\mathrm{M-T}
        =
        c\phs{1} \vek{A}\phs{1}_\mathrm{M-T} 
        +
        \sum_{r=2}^{N}
        c\phs{r}
        \vek{A}\phs{r}_\mathrm{dil}
        \vek{A}\phs{1}_\mathrm{M-T}
        =
        \left( 
            c\phs{1} \vek{I}
            +
            \sum_{r=2}^{N}
            c\phs{r}
            \vek{A}\phs{r}_\mathrm{dil}              
        \right)
        \vek{A}\phs{1}_\mathrm{M-T}
        \\
        \vek{A}\phs{1}_\mathrm{M-T}
        & =
        \left( 
            c\phs{1} \vek{I}
            +
            \sum_{r=2}^{N}
            c\phs{r}
            \vek{A}\phs{r}_\mathrm{dil}              
        \right)^{-1}
    \end{align*}

    \item \emph{Explicit} final formula
    %
    \begin{align*}
        \vek{L}^\mathrm{eff}_\mathrm{M-T}
        & =
        \vek{L}\phs{1}
        +
        \sum_{r=2}^N
        c\phs{r} \left( \vek{L}\phs{r} - \vek{L}\phs{1} \right)
        \vek{A}\phs{r}_\mathrm{dil}
        \left( 
            c\phs{1} \vek{I}
            +
            \sum_{s=2}^{N}
            c\phs{s}
            \vek{A}\phs{s}_\mathrm{dil}              
        \right)^{-1} \\
        \vek{A}\phs{r}_\mathrm{dil}
        & =
        \left( 
            \vek{I}
            +
            \vek{P}\phs{1, r} \left( \vek{L}\phs{r} - \vek{L}\phs{1} \right)
            \right)^{-1}
    \end{align*}

    \item Filter test: $c\phs{t} = 1$ for $t = 2, \ldots, N \Rightarrow c\phs{1} = 0 \Rightarrow \vek{L}^\mathrm{eff}_\mathrm{M-T} = \vek{L}\phs{t} \checkmark$

    \item \paragraph{Example} Isotropic phases, spherical inclusions, $N=2$: $\vek{L}^\mathrm{eff}_\mathrm{M-T} = L^\mathrm{eff}_\mathrm{M-T} \vek{I}$
    %
    \begin{align*}
        A\phs{2}_\mathrm{dil}
        & = 
        \frac{3 L\phs{1}}{2 L\phs{1} + L\phs{2}}
        \\
        L^\mathrm{eff}_\mathrm{M-T}
        & =
        L\phs{1}
        +
        c\phs{2} \left( L\phs{2} - L\phs{1} \right)
        A\phs{2}_\mathrm{dil}
        \left( 
            c\phs{1} 
            +
            c\phs{2}
            A\phs{2}_\mathrm{dil}              
        \right)^{-1}  
        \\
        & =
        L\phs{1}
        +
        c\phs{2} \left( L\phs{2} - L\phs{1} \right)
        \frac{3 L\phs{1}}{2 L\phs{1} + L\phs{2}}
        \left(
            \frac{
                c\phs{1}
                ( 2 L\phs{1} + L\phs{2} )
                +
                3 c\phs{2} L\phs{1}}
                {2 L\phs{1} + L\phs{2}}
        \right)^{-1}
        \\
        & =
        L\phs{1}
        +
        c\phs{2} \left( L\phs{2} - L\phs{1} \right)
        \frac{%
        3 L\phs{1}
        }{%
        (2 + c\phs{2}) L\phs{1} + c\phs{1} L\phs{2} 
        }
    \end{align*}    
%
    \item Dimensionless factor $\rho = L\phs{2} / L\phs{1}$
    %
    \begin{align*}
        \frac{L^\mathrm{eff}_\mathrm{M-T}}{L\phs{1}}
        & =
        1 
        +
        3 c\phs{2}
        \frac{\rho - 1}{%
        2 + c\phs{2} + (1 - c\phs{2} ) \rho
        }
        =
        \frac{%
        2 + \rho + c\phs{2}( 1 - \rho ) 
        +
        3 c\phs{2}
        ( \rho - 1 )
        }{%
        2 + \rho + c\phs{2}( 1 - \rho )
        }
        \\
        & =
        \frac{%
        2 + \rho - 2 c\phs{2}( 1 - \rho ) 
        }{%
        2 + \rho + c\phs{2}( 1 - \rho )
        }
    \end{align*}

    \item Recall Maxwell result~\cite{maxwell_treatise_1873} from Lecture~I (in the current notation)
    %
    \begin{align*}
        \frac{L^\mathrm{eff}_\mathrm{M}}{L\phs{1}}
        =
        \frac{1 + 2 c\phs{2} \beta}{1 - c\phs{2} \beta},
        &&
        \beta
        =
        \frac{L\phs{2} - L\phs{1}}{L\phs{2} + 2 L\phs{1}},
        &&
        c\phs{2} \ll 1
    \end{align*}

    \item Observe that 
    %
    \begin{align*}
        \beta 
        & =
        \frac{%
        \rho - 1
        }{%
        \rho + 2 
        }
        =
        -
        \frac{1 - \rho}{2 + \rho},
        \\
        \frac{L^\mathrm{eff}_\mathrm{M}}{L\phs{1}}
        & =
        \frac{
            1 - 2 c\phs{2} \frac{1 - \rho}{2 + \rho}
        }{
            1 + c\phs{2} \frac{1 - \rho}{2 + \rho}
        }
        =
        \frac{2 + \rho - 2 c\phs{2} (1 - \rho)}{%
        2 + \rho + c\phs{2} (1 - \rho)
        }.
    \end{align*}
    %
    Therefore, the Mori-Tanaka prediction coincides with the Maxwell formula and the dilute assumptions in the Maxwell derivation was overly pessimistic.

    \item First applications to elasticity by Mori and Tanaka~\cite{Mori1973} and Benveniste~\cite{Benveniste1987}.
\end{itemize}

\paragraph{Homework ideas}

\begin{itemize}\noitemsep
    \item Develop all formula for $L\phs{2} = 0$) and compare them with Voigt-Reuss bounds.
\end{itemize}

\Reference

\Acknowledgement 

\end{document}
 