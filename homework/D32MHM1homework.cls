\NeedsTeXFormat{LaTeX2e}
\ProvidesClass{D32MHM1homework}[2022/23/02 Homework assigments for course D32MHM1]

\LoadClass[11pt]{article}

%% Required packages
\RequirePackage[a4paper,margin=2cm]{geometry}
\RequirePackage[utf8]{inputenc}
\RequirePackage[numbers]{natbib}
\RequirePackage{graphicx}
\RequirePackage[colorlinks=true,linkcolor=black,citecolor=blue,urlcolor=blue]{hyperref}
\RequirePackage{fancyhdr}
\RequirePackage{pdfpages}
\RequirePackage{amsmath}
\RequirePackage{amsfonts}

%% Bibliography setup
\bibliographystyle{elsarticle-harv}

%% Graphicx setup
\graphicspath{{figures/}}

%% Other commands
\newcommand{\noitemsep}{\itemsep=0pt}

\newcounter{SessionNumber}

\renewcommand{\maketitle}[1]{%
   \setcounter{SessionNumber}{#1}%
    %% Facyhdr setup
    \pagestyle{fancy}%
    \fancyhf{}%
    \chead{Micromechanics of Heterogeneous Materials I - D32MHM1 | Homework assigment No.~\arabic{SessionNumber}}%
    \rfoot{\thepage}%
}

\newcommand{\Problem}[1]{%
    \paragraph{Problem}%
    #1%
}

\newcommand{\Hint}[1]{%
\medskip%
\par\noindent\emph{Hint.}~#1%
}

\newcommand{\References}{%
\bibliography{liter}
}

\newcommand{\Request}{%
\paragraph{Request} Plese feel free to submit any errors or recommendations using the issue tracker system at \url{https://gitlab.com/open-mechanics/teaching/d32_hm1_en/-/issues}\texttt{>New issue} (requires registration) or this \href{incoming+open-mechanics-teaching-d32-hm1-en-26728033-4unlmwfpk96rfyh3eissxpb0h-issue@incoming.gitlab.com}{email address}.
}
