\documentclass{MHM1_lectures}

\newcommand{\vek}[1]{{\boldsymbol #1}}
\newcommand{\phs}[1]{^{(#1)}}
\newcommand{\fl}{'} % Fluctuation
\newcommand{\de}{d}

\begin{document}
    
\maketitle{3}{Homogenization, apparent and effective properties}

\tableofcontents

\section{Homogenization via averaging}

\begin{itemize}\noitemsep
    \item Subject $\Omega$ to average intensity $\vek{E}$ or flux $\vek{J}$
    \item Decomposition of intensity and flux fields
    %
    \begin{align*}
        \vek{e}( \vek{x} )
        =
        \vek{E}
        +
        \vek{e}\fl( \vek{x} )
        &&  
        \vek{\jmath}( \vek{x} )
        =
        \vek{J}
        +
        \vek{\jmath}\fl( \vek{x} )
        && 
        \text{for } \vek{x} \in \Omega
    \end{align*}
    %
    where the \emph{fluctuations} $\vek{e}\fl \in \mathcal{E}\fl$ and $\vek{\jmath}\fl \in \mathcal{J}\fl$ satisfy \emph{linear} PDEs
    %
    \begin{align*}
        - \vek{\nabla}^T 
        \left( 
            \vek{L}( \vek{x} )
            \vek{e}\fl (\vek{x} )
        \right)
        & =
        \vek{\nabla}^T 
        \left( 
            \vek{L}( \vek{x} )
            \vek{E} 
        \right)
        & \text{for }
        \vek{x} \in \Omega
        \\
        - \vek{\nabla}\times
        \left( 
            \vek{M}( \vek{x} )
            \vek{\jmath}\fl (\vek{x} )
        \right)
        & =
        \vek{\nabla} \times
        \left( 
            \vek{M}( \vek{x} )
            \vek{J} 
        \right)
        & \text{for }
        \vek{x} \in \Omega
    \end{align*}

    \item Fluctuating intensity and flux localization factors $\vek{A}\fl : \Omega \rightarrow \mathbb{R}^{d \times d}$ and $\vek{B}\fl : \Omega \rightarrow \mathbb{R}^{d \times d}$
    %
    \begin{align*}
        \vek{e}\fl( \vek{x} )
        =
        \vek{A}\fl( \vek{x} ) \vek{E}
        &&
        \vek{\jmath}\fl( \vek{x} )
        =
        \vek{B}\fl( \vek{x} ) \vek{J}
        && 
        \text{ for }
        \vek{x} \in \Omega        
    \end{align*}

    \item Flux averaging
    %
    \begin{align*}
        \vek{J} 
        & =
        \langle \vek{\jmath}( \vek{x}) \rangle
        =
        \frac{1}{|\Omega|}
        \int_\Omega
        \vek{\jmath}( \vek{x} ) 
        \de \vek{x}
        =
        \frac{1}{|\Omega|}
        \int_\Omega
        \vek{L}( \vek{x})
        \vek{e}( \vek{x} ) 
        \de \vek{x}
        =
        \frac{1}{|\Omega|}
        \int_\Omega
        \vek{L}( \vek{x})
        \left( \vek{E} + \vek{e}\fl( \vek{x} ) \right) 
        \de \vek{x}
        \\
        & = 
        \frac{1}{|\Omega|}
        \int_\Omega
        \vek{L}( \vek{x})
        \left( \vek{E} + \vek{A}\fl( \vek{x} ) \vek{E}  \right) 
        \de \vek{x}
        =
        \frac{1}{|\Omega|}
        \int_\Omega
        \vek{L}( \vek{x})
        \left( \vek{I} + \vek{A}\fl(\vek{x})  \right) 
        \de \vek{x}
        \vek{E}  
        =
        \left\langle
        \vek{L}( \vek{x})
        \left( \vek{I} + \vek{A}\fl(\vek{x})  \right)
        \right\rangle
        \vek{E}
        \\  
        & =
        \vek{L}^\mathrm{hmg} \vek{E}
    \end{align*}

    \item By analogy
    %
    \begin{align*}
        \vek{E}
        =
        \vek{M}^\mathrm{hmg}
        \vek{J}
        &&
        \vek{M}^\mathrm{hmg}
        =
        \left\langle
        \vek{M}( \vek{x})
        \left( \vek{I} + \vek{B}\fl(\vek{x})  \right)
        \right\rangle
    \end{align*}

    \item Boundary conditions to be specified later
\end{itemize}

\section{Homogenization via variational principles}

\begin{itemize}\noitemsep

    \item It follows from Lecture~II that
    %
    \begin{align*}
        W( \vek{E} + \vek{e}\fl )
        \leq 
        W( \vek{E} + \vek{v} \fl )
        \text{ for any }
        \vek{v}\fl \in \mathcal{E}\fl
    \end{align*}

    \item By energy equivalence
    %
    \begin{align*}
    W( \vek{E} + \vek{e}\fl )
    =
    | \Omega | 
    \frac{1}{2}
    \vek{E}^T \vek{L}^\mathrm{hmg} \vek{E}
    =
    | \Omega |
    W^\mathrm{hmg}( \vek{E} )
    &&
    \vek{J} = \frac{ \partial {W}^\mathrm{hmg}(\vek{s}) }{\partial \vek{s}}
    \left( \vek{E} \right)
    && 
    \vek{L}^\mathrm{hmg}
    =
    \frac{ \partial^2 {W}^\mathrm{hmg}(\vek{s}) }{\partial \vek{s}^2}
    \end{align*}

    \item Therefore
    %
    \begin{align*}
    W^\mathrm{hmg}( \vek{E} )
    & =
    \frac{1}{2}
    \vek{E}^T \vek{L}^\mathrm{hmg} \vek{E}    
    \leq 
    \frac{1}{| \Omega |}
    \int_\Omega
    w\left( \vek{x}, \vek{E} + \vek{v}\fl ( \vek{x}) \right)
    \de \vek{x}
    \\
    & =
    \frac{1}{2}
    \left\langle
    \left( \vek{E} + \vek{v}\fl ( \vek{x}) \right)^T
    \vek{L}( \vek{x} )
    \left( \vek{E} + \vek{v}\fl ( \vek{x}) \right)    
    \right\rangle
    \text{ for all }
    \vek{v}\fl \in \mathcal{E}\fl
    \end{align*}

    \item By analogy 
    %
    \begin{align*}
        W^{*,\mathrm{hmg}}( \vek{J} )
        \leq 
        \frac{1}{2}
    \left\langle
    \left( \vek{J} + \vek{\jmath}\fl ( \vek{x}) \right)^T
    \vek{M}( \vek{x} )
    \left( \vek{J} + \vek{\jmath}\fl ( \vek{x}) \right)    
    \right\rangle
    \text{ for all }
    \vek{\jmath}\fl \in \mathcal{J}\fl
    \end{align*}
    %
    and
    %
    \begin{align*}
        \vek{E} = \frac{ \partial {W}^{*,\mathrm{hmg}}(\vek{s}) }{\partial \vek{s}}
        \left( \vek{J} \right),
        && 
        \vek{M}^\mathrm{hmg}
        =
        \frac{ \partial^2 {W}^{*,\mathrm{hmg}}(\vek{s}) }{\partial \vek{s}^2}        
    \end{align*}

    \item Boundary conditions to be specified later
    
\end{itemize}

\section{Equivalence}

\begin{itemize}
    \item Result by J. Vondřejc~\cite[Appendix~A]{Vondrejc:2015:GUL}: If $\mathcal{E}\fl$ and $\mathcal{J}\fl$ satisfy the \emph{Helmholtz decomposition} then 
    %
    \begin{align*}
        \vek{L}^\mathrm{hmg}
        =
        \left( 
            \vek{M}^\mathrm{hmg}
        \right)^{-1}
    \end{align*}
    %
    (unsure whether the reverse implication holds).

    \item Hence, the homogenized properties depend on
    %
    \begin{itemize}
        \item $\Omega$~(size-dependence)
        \item The adopted boundary conditions
    \end{itemize}

\end{itemize}

\section{Apparent properties~\cite{Huet:1990:AVC}}

Finite-size $\Omega$: hmg $\rightarrow$ app

\begin{itemize}
    \item Primal and dual problems
    \item Recall from Lecture~II
    \begin{itemize}
    \item Two types of boundary conditions (Dirichlet and Neumann)
    \item Two variants of Helmholtz decomposition
    \end{itemize}
\end{itemize}

\begin{tabular}{lcc}
    &  Dirichlet & Neumann \\
Primal & 
$\vek{L}^{\mathrm{app}}_{\Omega, \mathrm{D}}$ &
$\vek{L}^{\mathrm{app}}_{\Omega}$ \\
Dual & 
$\vek{M}^{\mathrm{app}}_{\Omega}$ &
$\vek{M}^{\mathrm{app}}_{\Omega, \mathrm{N}}$ \\
\\
\end{tabular}

where, e.g.
%
$\vek{L}^{\mathrm{app}}_{\heartsuit}$ corresponds to primal homogenized properties while adopting boundary conditions from $\mathcal{E}\fl_\heartsuit$

\begin{itemize}
    \item By the Helmholtz decomposition
    %
    \begin{align*}
        \vek{L}^{\mathrm{app}}_{\Omega, \mathrm{D}}
        =
        \left( \vek{M}^{\mathrm{app}}_{\Omega} \right)^{-1},
        &&
        \vek{L}^{\mathrm{app}}_{\Omega}
        =
        \left( \vek{M}^{\mathrm{app}}_{\Omega, \mathrm{N}} \right)^{-1}
    \end{align*}
    
    \item By variational arguments
    %
    \begin{align*}
        W^\mathrm{hmg}_{\Omega, \mathrm{D}} ( \vek{E} ) 
        & \leq
        \frac{1}{|\Omega|}
        W( \vek{E} +  \vek{v} ')
        \text{ for any }
        \vek{v}' \in \mathcal{E}'_{\Omega, \mathrm D}
        \\
        W^\mathrm{hmg}_{\Omega} ( \vek{E} ) 
        & \leq
        \frac{1}{|\Omega|}
        W( \vek{E} +  \vek{v} ')
        \text{ for any }
        \vek{v}' \in \mathcal{E}'_{\Omega}
    \end{align*}

    \item Because $\mathcal{E}'_{\Omega, \mathrm D} \subset \mathcal{E}'_{\Omega}$
    %
    \begin{align*}
    W^\mathrm{hmg}_{\Omega} ( \vek{E} )
    & \leq 
    W^\mathrm{hmg}_{\Omega, \mathrm{D}} ( \vek{E} )
    \text{ for any }
    \vek{E} \in \mathbb{R}^d \\
    \frac{1}{2} 
    \vek{E}^T
    \vek{L}^{\mathrm{app}}_{\Omega}
    \vek{E} & \leq
    \frac{1}{2} 
    \vek{E}^T
    \vek{L}^{\mathrm{app}}_{\Omega, \mathrm{D}}
    \vek{E}
    \text{ for any }
    \vek{E} \in \mathbb{R}^d \\
    \vek{L}^{\mathrm{app}}_{\Omega}
    & \preceq   
    \vek{L}^{\mathrm{app}}_{\Omega, \mathrm{D}}
    \end{align*}
    
    \item By analogy
    %
    \begin{align*}
        \vek{M}^{\mathrm{app}}_{\Omega}
        \preceq   
        \vek{M}^{\mathrm{app}}_{\Omega, \mathrm{N}}
        \Leftrightarrow
        \left( \vek{M}^{\mathrm{app}}_{\Omega} \right)^{-1}
        \succeq   
        \left( \vek{M}^{\mathrm{app}}_{\Omega, \mathrm{N}} \right)^{-1}     
    \end{align*}
    
    \item By the Helmholtz decomposition
    %
    \begin{align*}
        \vek{L}^{\mathrm{app}}_{\Omega}    
        =
        \left( \vek{M}^{\mathrm{app}}_{\Omega, \mathrm{N}} \right)^{-1}
        & \preceq
        \left( \vek{M}^{\mathrm{app}}_{\Omega} \right)^{-1}
        =
        \vek{L}^{\mathrm{app}}_{\Omega, \mathrm{D}}
        \\
        \vek{M}^{\mathrm{app}}_{\Omega}    
        =
        \left( \vek{L}^{\mathrm{app}}_{\Omega, \mathrm{N}} \right)^{-1}
        & \preceq
        \left( \vek{L}^{\mathrm{app}}_{\Omega} \right)^{-1}
        =
        \vek{M}^{\mathrm{app}}_{\Omega, \mathrm{N}}    
    \end{align*}
    %
    (the information content of primal and dual formulation is the same)
\end{itemize}

\section{Effective properties}

\begin{itemize}

\item For $\Omega \equiv \mathbb{R}^d$: hmg $\rightarrow$ eff 
\item One variant of Helmholtz decomposition~(no boundary conditions)
%
\begin{align*}
\vek{L}^\mathrm{eff} = \left( \vek{M}^\mathrm{eff} \right)^{-1}
\end{align*}

\item Genuine \emph{material} properties

\item Under rather general assumptions
%
\begin{align*}
    \vek{L}^{\mathrm{eff}}
    \leftarrow 
    \vek{L}^{\mathrm{app}}_{\Omega}
    & \preceq   
    \vek{L}^{\mathrm{app}}_{\Omega, \mathrm{D}}
    \rightarrow 
    \vek{L}^{\mathrm{eff}}
    \text{ as } \Omega \rightarrow \mathbb{R}^d
    \\
    \vek{M}^{\mathrm{eff}}
    \leftarrow 
    \vek{M}^{\mathrm{app}}_{\Omega}
    & \preceq   
    \vek{M}^{\mathrm{app}}_{\Omega, \mathrm{N}}
    \rightarrow 
    \vek{M}^{\mathrm{eff}}
    \text{ as } \Omega \rightarrow \mathbb{R}^d
\end{align*}

\end{itemize}

\section{Up-scaling}

\begin{itemize}

\item In our applications, $\Omega$ represents a material point~(microscale problem)

\item After zoom-out $\Omega^\mathrm{M}$~(macro-problem)
%
\begin{align*}
    - \vek{\nabla}^T_{\vek X} 
    \left( 
        \vek{L}^\mathrm{eff}
        \vek{\nabla}_{\vek X}
        U ( \vek{X} )
    \right) 
    =
    f( \vek{X} )
    \text{ for }
    \vek{X} \in \Omega^\mathrm{M}
    + 
    \text{boundary conditions }
\end{align*}
%
$\vek{L}^\mathrm{eff}$ may possibly depend on $\vek{X}$.

\item Valid approximation, iff \emph{separation of scales}~\cite{Hill:1963:EPS} holds

\begin{itemize}
    \item $\Omega$ is large enough~(Representative Volume Element (RVE))
    \item Gradient of $U$ is approximately linear over (real-sized) RVE 
\end{itemize}
%
\begin{align*}
    \ell \ll \ell_\mathrm{RVE} \ll \ell_\vek{E}
\end{align*}

\item From now, we will adopt the assumption and drop the depedence on $\Omega$ and boundary conditions

\end{itemize}

\paragraph{Homework ideas}

\begin{itemize}\noitemsep
    \item Relation to more engineering approaches (take from Petr Kabele)
    \item Derive Dvorak identifies for \emph{full} concentration factors 
    \item Some computations of the L\"{o}wner partial order
\end{itemize}

\Reference

\Acknowledgement 

\end{document}
