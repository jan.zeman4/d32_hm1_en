\documentclass{MHM1_lectures}

\newcommand{\vek}[1]{{\boldsymbol #1}}
\newcommand{\phs}[1]{^{(#1)}}
\newcommand{\fl}{'} % Fluctuation
\newcommand{\de}{d}
\newcommand{\ft}[1]{\widehat{#1}}
\newcommand{\imu}{\mathrm{i}\,}
\newcommand{\trn}{^T}

\begin{document}
    
\maketitle{6}{Advanced theory of effective properties~II: Eshelby problem, equivalent inclusion problem}

\tableofcontents

\section{Eshelby problem -- solution}

\begin{itemize}\noitemsep

     \item Recall the form of the Eshelby problem
    %
    \begin{align*}
        \vek{e}_\mathrm{I}( \vek{x} )
        =
        \vek{\nabla}_\vek{x}
        \int_{\mathbb{R}^d}
        G\phs{0}( \vek{x} - \vek{y} )  
        \vek{\nabla}_{\vek y}\trn
        \left[ 
        \chi_{\Omega_\mathrm{I}}( \vek{y} )
        \vek{p}
        \right] 
        \de \vek{y}
    \end{align*}
 
    \item Apply the by parts integration 
    %
    \begin{align*}
        \int_{\mathbb{R}^d}
        G\phs{0}( \vek{x} - \vek{y} )  
        \vek{\nabla}_{\vek y}\trn 
        \left[
        \chi_{\Omega_\mathrm{I}}( \vek{y} )
        \vek{p}
        \right] 
        \de \vek{y}
        & =
        -
        \int_{\mathbb{R}^d}
        \left( 
        \vek{\nabla}_{\vek y}
        G\phs{0}( \vek{x} - \vek{y} ) 
        \right)\trn 
        \chi_{\Omega_\mathrm{I}}( \vek{y} )
        \vek{p} 
        \de \vek{y}
        \\
        & =
        \int_{\mathbb{R}^d}
        \left( 
            \vek{\nabla}_{\vek x}
            G\phs{0}( \vek{x} - \vek{y} ) 
        \right)\trn 
        \chi_{\Omega_\mathrm{I}}( \vek{y} )
        \vek{p}
        \de \vek{y}
        \vek{p}
    \end{align*} 

    \item Hence
    %
    \begin{align*}
        \vek{e}_\mathrm{I}( \vek{x} )
        =
        \int_{\mathbb{R}^d}
            \vek{\nabla}^2_{\vek x}
            G\phs{0}( \vek{x} - \vek{y} )
            \chi_{\Omega_\mathrm{I}}( \vek{y} )
            \vek{p}             
        \de \vek{y}
        =
        -
        \int_{\mathbb{R}^d}
            \vek{\Gamma}\phs{0}( \vek{x} - \vek{y} ) 
            \chi_{\Omega_\mathrm{I}}( \vek{y} ) 
            \vek{p}
        \de \vek{y}    
        =
        -
        \vek{P}_{\mathrm{I}}\phs{0}( \vek{x} )
        \vek{p}
    \end{align*}
    %
    where $\vek{\Gamma}\phs{0}$ is the Willis function~\cite{Willis1977} and $\vek{P}\phs{0}_{\mathrm{I}}$ is the Walpole matrix~\cite{Walpole1966} given by
    %
    \begin{align*}
        \vek{P}_{\mathrm{I}}\phs{0}( \vek{x} )
        =
        \int_{\Omega_\mathrm{I}}
            \vek{\Gamma}\phs{0}( \vek{x} - \vek{y} ) 
        \de \vek{y}.    
    \end{align*}

    \item Recall from Lecture~V that
    %
    \begin{align*}
        G\phs{0}( \vek{x} - \vek{y} )
        =
        \frac{1}{8 \pi^2}
        \int_{\partial S^3}
        \left( 
            \vek{\omega}\trn 
            \vek{L}\phs{0}
            \vek{\omega} 
        \right)^{-1}
        \delta\bigl( \vek{\omega}\trn \left( \vek{x} - \vek{y} \right) \bigr)
        \de\vek{\omega}
    \end{align*}

    \item Therefore
    %
    \begin{align*}
        \vek{P}_\mathrm{I}\phs{0}( \vek{x} ) 
        =
        -
        \frac{1}{8 \pi^2}
        \int_{\partial S^3}
        \left( 
            \vek{\omega}\trn 
            \vek{L}\phs{0}
            \vek{\omega} 
        \right)^{-1}
        \vek{\nabla}^2_{\vek x}
        \underbrace{
        \int_{\Omega_\mathrm{I}}
        \delta\bigl( \vek{\omega}\trn \left( \vek{x} - \vek{y} \right) \bigr)
        \de{\vek y}
        }_{\psi_{\mathrm I}( \vek{x}, \vek{\omega} )}
        \de{\vek \omega}
    \end{align*}
    %
    \begin{center}
        \includegraphics[scale=0.7]{figures/06-sphere}
    \end{center}

    \item "Easily" shown by Laws~\cite{Laws:1977:DSS} and Willis~\cite{Willis1977} that for an ellipsoid centered at origin in the form~(for isotropic elasticity)
    %
    \begin{align*}
        \Omega_\mathrm{I} 
        =
        \left\{
            \vek{y} \in \mathbb{R}^d :
            \vek{y}\trn \vek{A} \vek{y} \leq 1
            \text{ for a given } \vek{A} \succ \vek{0} 
        \right\},
    \end{align*}
    %
    and $\vek{x} \in \Omega_\mathrm{I}$
    %
    \begin{align*}
        \psi_{\mathrm I}( \vek{x}, \vek{\omega} )
        =
        \frac{\pi}{t^3( \vek{\omega} ) \sqrt{\alpha}}
        \left( 
            t^2( \vek{\omega } ) - \left( \vek{x}\trn \vek{\omega} \right)^2
        \right),
        &&
        \alpha = \det \vek{A}, 
        &&
        t^2( \vek{\omega} ) = \vek{\omega}\trn \vek{A}^{-1} \vek{\omega}
    \end{align*}

    \item Therefore
    %
    \begin{align*}
        \vek{\nabla}^2_{\vek x}
        \psi_{\mathrm I}( \vek{x}, \vek{\omega} )
        =
        -\frac{2\pi}{t^3( \vek{\omega} ) \sqrt{\alpha}}
        \vek{\omega}\vek{\omega}\trn
    \end{align*}
    %
    and, finally, 
    %
    \begin{align*}
        \vek{P}_\mathrm{I}\phs{0}( \vek{x} )
        =
        \vek{P}_\mathrm{I}\phs{0}
        =
        \frac{1}{4 \pi \sqrt{\alpha}}
        \int_{\partial S^3}
        \frac{1}{t^3( \vek{\omega} )}
        \frac{\vek{\omega} \vek{\omega\trn}}{%
        \vek{\omega}\trn \vek{L}\phs{0} \vek{\omega}}
        \de{\vek \omega}
    \end{align*}
    %
    demonstrating the presence of \emph{uniform} fields inside the inclusion. Proven by~\cite{Liu2007} that ellipsoid is the only inclusion having this property~(for isotropic elasticity).

    \item Solved for several cases of practical interest by Hiroshi and Taya~\cite{Hiroshi1986} for $\vek{A} = \text{diag}\,[ (a_1)^{-2}, (a_2)^{-2}, (a_3)^{-2}]$ and isotropic $\vek{L}\phs{0} = L\phs{0} \vek{I}$
    %
    \begin{itemize}
        \item Sphere ($a_1 = a_2 = a_3$)
        %
        \begin{align*}
            P\phs{0}_{11} = P\phs{0}_{22} = P\phs{0}_{33} = \frac{1}{3 L\phs{0}}
        \end{align*}
        
        \item Elliptic cyllinder ($a_3 \rightarrow \infty$)
        %
        \begin{align*}
            P\phs{0}_{11} = \frac{a_2}{L\phs{0}(a_1 + a_2)},
            &&
            P\phs{0}_{22} = \frac{a_1}{L\phs{0}(a_1 + a_2)},
            &&
            P\phs{0}_{33} = 0
        \end{align*}
    
        \item Penny-shape~($a_1 = a_2 \gg a_3$)
        %
        \begin{align*}
            P\phs{0}_{11} = P\phs{0}_{22} = \frac{\pi a_3}{4L\phs{0} a_1}, &&
            P\phs{0}_{33} = \frac{1}{L\phs{0}} \left( 1 - \frac{\pi a_3}{2 a_1} \right)
        \end{align*}
    
        \item Oblate spheroid~($a_1 = a_2  > a_3$)
        %
        \begin{align*}
            P\phs{0}_{11} = P\phs{0}_{22} = \frac{a_1^2 a_3}{2L\phs{0} (a_1^2 - a_3^2)^{3/2}}
            \left\{ 
                \cos^{-1} \frac{a_3}{a_1}
                -
                \frac{a_3}{a_1}
                \sqrt{1 - \left( \frac{a_3}{a_1}\right)^2 }
            \right\}, &&
            P\phs{0}_{33} = \frac{1}{L\phs{0}} - 2 P\phs{0}_{11} 
        \end{align*}
    
        \item Prolate spheroid~($a_1 = a_2 < a_3$)
    
        \begin{align*}
            P\phs{0}_{11} = P\phs{0}_{22} = \frac{a_1^2 a_3}{2L\phs{0} (a_1^2 - a_3^2)^{3/2}}
            \left\{ 
                \frac{a_3}{a_1}
                \sqrt{\left( \frac{a_3}{a_1}\right)^2 - 1}
                -
                \cosh^{-1} \frac{a_3}{a_1}
            \right\}, &&
            P\phs{0}_{33} = \frac{1}{L\phs{0}} - 2 P\phs{0}_{11} 
        \end{align*}
    \end{itemize}
    %
    all other components are zero. Note that all results depend only on the ratii $a_i / a_j$, therefore $\vek{P}\phs{0}_{\mathrm{I}}$ depends only on shape, but not on size.
    
\end{itemize}

\section{Equivalent inclusion method}

\begin{itemize}
    \item Heterogeneous inclusion with $\vek{L}_{\mathrm I} \neq \vek{L}\phs{0}$ subjected to $\vek{E}$ 
    \item Equivalent polarization $\vek{p}$ inside the inclusion: requires the same intensity fields and fluxes inside~$\Omega_{\mathrm I}$
\end{itemize}

\centering{%
\begin{tabular}{ccc}
    & Inclusion & Heterogeneity \\
 Intensity & $\vek{E} - \vek{P}\phs{0}_\mathrm{I} \vek{p}$ & $\vek{e}$ \\
 Flux      & $\vek{L}\phs{0} \vek{e} + \vek{p}$ & $\vek{L}_\mathrm{I} \vek{e}$ 
\end{tabular}
}

\begin{align*}
    \vek{L}\phs{0} \vek{e} + \vek{p}
    & =
    \vek{L}_I\vek{e}
    \Rightarrow 
    \vek{p} 
    =
    ( \vek{L}_\mathrm{I} - \vek{L}\phs{0} ) \vek{e}
    \\
    \vek{E} 
    -
    \vek{P}\phs{0}_\mathrm{I} 
    ( \vek{L}_\mathrm{I} - \vek{L}\phs{0} ) \vek{e} & = \vek{e}
    \Rightarrow
    \vek{e} = \left[ \vek{I} + \vek{P}\phs{0}_\mathrm{I}( \vek{L}_\mathrm{I} - \vek{L}\phs{0} ) \right]^{-1} \vek{E}
    =
    \vek{A}\phs{0}_\mathrm{I} \vek{E}
\end{align*}

\begin{itemize}

\item Examples: Spherical inclusion in the spherial matrix: $\vek{A}\phs{0}_\mathrm{I} = A\phs{0}_\mathrm{I} \vek{I}$ with
%
\begin{align*}
A\phs{0}_\mathrm{I} 
= 
\left( 
    1 
    + 
    \frac{1}{3 L\phs{0}}( L_\mathrm{I} - L\phs{0} )
\right)^{-1}
=
\left( 
    \frac{2 L\phs{0} + L_\mathrm{I}}{3 L\phs{0}}
\right)^{-1} 
=
\frac{3 L\phs{0}}{2 L\phs{0} + L_\mathrm{I}}
\end{align*}

\item Introduced by Eshelby~\cite{Eshelby:1957:DEF}~(for elasticity).

\end{itemize}

\paragraph{Homework ideas}

\begin{itemize}\noitemsep
    \item Consider the original Eshelby setup~\cite{Eshelby:1957:DEF} with an initial gradient $\vek{g}$.
    \item Express the Walpole matrix using the Fourier transform of $\vek{\Gamma}^{(0)}$
    \item Evaluate the $\vek{P}$ matrix for sphere.
\end{itemize}

\Reference

\Acknowledgement 

\end{document}