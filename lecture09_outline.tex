\documentclass{MHM1_lectures}

\newcommand{\vek}[1]{{\boldsymbol #1}}
\newcommand{\phs}[1]{^{(#1)}}
\newcommand{\fl}{'} % Fluctuation
\newcommand{\de}{d}
\newcommand{\ft}[1]{\widehat{#1}}
\newcommand{\imu}{\mathrm{i}\,}
\newcommand{\trn}{^T}

\begin{document}
    
\maketitle{9}{Hashin-Shtrikman bounds}

\tableofcontents

\section{Stochastic variational principles}

\begin{itemize}\noitemsep
    \item Recall from Lecture~III that
    %
    \begin{align*}
        \vek{E}\trn \vek{L}^\mathrm{eff} \vek{E}
        & =
        \min_{\vek{v}\fl \in \mathcal{E}\fl} 
        \left\langle
        \left( \vek{E} + \vek{v}\fl ( \vek{x}) \right)\trn
        \vek{L}( \vek{x} )
        \left( \vek{E} + \vek{v}\fl ( \vek{x}) \right)    
        \right\rangle
        \\
        \vek{L}( \vek{x} )
        & =
        \sum_{r=1}^{N} \chi\phs{r}( \vek{x} ) \, \vek{L}\phs{r}
    \end{align*}  

    \item Consider realization $\alpha \in \mathcal{S}$
    %
    \begin{align}
        \vek{E}\trn \vek{L}^\mathrm{eff}( \alpha ) \vek{E}
        & =
        \min_{\vek{v}\fl( \alpha ) \in \mathcal{E}\fl} 
        \left\langle
        \left( \vek{E} + \vek{v}\fl ( \vek{x}; \alpha ) \right)\trn
        \vek{L}( \vek{x}; \alpha )
        \left( \vek{E} + \vek{v}\fl ( \vek{x}; \alpha ) \right)    
        \right\rangle
        \label{eq:Leff_real_dependent}
        \\
        \vek{L}( \vek{x}; \alpha )
        & =
        \sum_{r=1}^{N} \chi\phs{r}( \vek{x}; \alpha ) \, \vek{L}\phs{r}        
    \end{align}

    \item Ensemble average of the left hand side of~\eqref{eq:Leff_real_dependent}
    %
    \begin{align*}
        \mathbb{E} \bigl[ \vek{E}\trn \vek{L}^\mathrm{eff}( \alpha ) \vek{E} \bigr]
        & =
        \sum_{\alpha \in \mathcal{S}}
        \vek{E}\trn \vek{L}^\mathrm{eff}( \alpha ) \vek{E}
        \mu( \alpha )
        =
        \vek{E}\trn
        \left[ 
            \sum_{\alpha \in \mathcal{S}}
            \vek{L}^\mathrm{eff}( \alpha )
            \mu( \alpha )
        \right]
        \vek{E}
        =
        \vek{E}\trn
        \mathbb{E} \bigl[ \vek{L}^\mathrm{eff}( \alpha ) \bigr]
        \vek{E}
        \\
        & :=
        \vek{E}\trn
        \vek{L}^\mathrm{eff}
        \vek{E}       
    \end{align*}

    \item No such operation is possible for the right hand side of~\eqref{eq:Leff_real_dependent}, hence
    %
    \begin{align}
        \vek{E}\trn
        \vek{L}^\mathrm{eff}
        \vek{E}       
        =
        \mathbb{E}\left[ 
            \min_{\vek{v}\fl( \alpha ) \in \mathcal{E}\fl} 
            \left\langle
            \left( \vek{E} + \vek{v}\fl ( \vek{x}; \alpha ) \right)\trn
            \vek{L}( \vek{x}; \alpha )
            \left( \vek{E} + \vek{v}\fl ( \vek{x}; \alpha ) \right)    
            \right\rangle    
        \right]
        \label{eq:Leff_def}
        &&
        \text{for any } \vek{E} \in \mathbb{R}^d       
    \end{align}

    \item- By analogy
    %
    \begin{align}
        \vek{J}\trn
        \vek{M}^\mathrm{eff}
        \vek{J}       
        & =
        \mathbb{E}\left[ 
            \min_{\vek{\jmath}\fl( \alpha ) \in \mathcal{J}\fl} 
            \left\langle
            \left( \vek{J} + \vek{q}\fl ( \vek{x}; \alpha ) \right)\trn
            \vek{M}( \vek{x}; \alpha )
            \left( \vek{J} + \vek{q}\fl ( \vek{x}; \alpha ) \right)    
            \right\rangle    
        \right]
        &&
        \text{for any } \vek{J} \in \mathbb{R}^d       
        \\
        \vek{M}( \vek{x}; \alpha )
        & =
        \sum_{r=1}^{N} 
        \chi\phs{r}( \vek{x}; \alpha) \, \vek{M}\phs{r}
        \label{eq:Meff_def}      
    \end{align}

\end{itemize}

\section{Voigt and Reuss one-point bounds}

\begin{itemize}
    \item Consider $\vek{v}\fl( \vek{x}; \alpha ) \equiv \vek{0}$ 
%
    \item It follows from~\eqref{eq:Leff_def} that
    %
    \begin{align*}
        \vek{E}\trn
        \vek{L}^\mathrm{eff}
        \vek{E}       
        \leq\, & 
        \mathbb{E}\left[ 
            \left\langle
            \vek{E}\trn
            \left(
                \sum_{r=1}^{N}
                \chi\phs{r}( \vek{x}; \alpha) 
                \vek{L}\phs{r}
            \right)
            \vek{E}     
            \right\rangle    
        \right]
        =
        \vek{E}\trn
        \left(
            \sum_{r=1}^{N}
            \mathbb{E}\left[
                \left\langle
                \chi\phs{r}( \vek{x}; \alpha)
                \right\rangle
            \right]
            \vek{L}\phs{r}
        \right)
        \vek{E}
        \\
        \, & =
        \vek{E}\trn
        \left(
            \sum_{r=1}^{N}
            \left\langle
            \mathbb{E}\left[
                \chi\phs{r}( \vek{x}; \alpha)
            \right]
            \right\rangle
            \vek{L}\phs{r}
        \right)
        \vek{E}
        =
        \vek{E}\trn
        \left(
            \sum_{r=1}^{N}
            \left\langle
            S\phs{r}( \vek{x} )
            \right\rangle
            \vek{L}\phs{r}
        \right)
        \vek{E}
        \\ 
        \, & 
        \stackrel{\text{SU}}{=}
        \vek{E}\trn
        \left(
            \sum_{r=1}^{N}
            S\phs{r}
            \vek{L}\phs{r}
        \right)
        \vek{E}
        \stackrel{\text{E}}{=}
        \vek{E}\trn
        \left(
            \sum_{r=1}^{N}
            c\phs{r}
            \vek{L}\phs{r}
        \right)
        \vek{E}
        =
        \vek{E}\trn
        \vek{L}^\mathrm{V}
        \vek{E}
        \Rightarrow 
        \vek{L}^\mathrm{eff}
        \preceq
        \vek{L}^\mathrm{V}
    \end{align*}

    \item By analogy, taking $\vek{\jmath}\fl( \vek{x}; \alpha ) \equiv \vek{0}$ in \eqref{eq:Meff_def} and considering statistically uniform ergodic media yields
    %
    \begin{align}
        \vek{M}^\mathrm{eff}
        \preceq
        \sum_{r=1}^{N}
        c\phs{r}
        \vek{M}\phs{r}
        =
        \vek{M}^\mathrm{R}
    \end{align}

    \item Depends only on $c\phs{r} \rightarrow$ one-point
\end{itemize}

\subsection{Three-point bounds}

\begin{itemize}
    \item Condtion $\vek{v}\fl( \alpha ) \in \mathcal{E}$ is challending to satisty
    
    \item Recall the Eshelby problem from Lecture~VI:
    %
    \begin{align*}
        \vek{e}_\mathrm{I}( \vek{x} )
        =
        -
        \int_{\mathbb{R}^d}
            \vek{\Gamma}\phs{0}( \vek{x} - \vek{y} ) 
            \chi_{\Omega_\mathrm{I}}( \vek{y} ) 
            \vek{p}
        \de \vek{y},    
    \end{align*}
    %
    where $\bullet\phs{0}$ denotes a quantity associated with a reference medium and $\vek{p}$ is polarization. 

    \item This suggests choosing the test field in the form
    %
    \begin{align}\label{eq:test_inclusion}
        \vek{v}\fl( \vek{x} )
        =
        -
        \sum_{r=1}^N
        \int_{\mathbb{R}^d}
            \vek{\Gamma}\phs{0}( \vek{x} - \vek{y} ) 
            \chi\phs{r}(\vek{y}; \alpha) 
            \vek{q}\phs{r}
        \de \vek{y},    
    \end{align}

    \item Consider the term $\vek{v}\fl(\vek{x}; \alpha)^T \chi\phs{r} (\vek{x}; \alpha) \vek{v}\fl(\vek{x}; \alpha)$ occuring in~\eqref{eq:Leff_def}
    %
    \begin{align*}
        \left( 
            \sum_{s=1}^N
            \int_{\mathbb{R}^d}
                \vek{\Gamma}\phs{0}( \vek{x} - \vek{y} ) 
                \chi\phs{s}(\vek{y}; \alpha) 
                \vek{q}\phs{s}
            \de \vek{y}        
        \right)^T
        \chi\phs{r} (\vek{x}; \alpha)
        \left(
            \sum_{t=1}^N
            \int_{\mathbb{R}^d}
                \vek{\Gamma}\phs{0}( \vek{x} - \vek{z} ) 
                \chi\phs{s}(\vek{z}; \alpha) 
                \vek{q}\phs{t}
            \de \vek{z}      
        \right)
    \end{align*}

    \item Aftery averaging, it would yield three-point bounds $\rightarrow$ new variational principle

\end{itemize}

\section{Hashin-Shtrikman variational principles~(deterministic version)}

\begin{itemize}
    \item Consider two homogenization problems with $\vek{L}(\vek{x}) \leq \vek{L}\phs{0}$
    
    \begin{align*}
        \vek{E}^T \vek{L}^\mathrm{eff} \vek{E}
        & \leq 
        \left\langle
        \vek{q}(\vek{x})^T 
        \left( \vek{L}\phs{0} - \vek{L}( \vek{x} )\right)^{-1}
        \vek{q}( \vek{x} )
        \right\rangle
        +
        \left\langle
        \left( \vek{E} + \vek{v}\fl(\vek{x}) \right)^T
        \vek{L}\phs{0}
        \left( \vek{E} + \vek{v}\fl(\vek{x}) \right)
        \right\rangle
        \\
        & +
        2 \left\langle
        \vek{q}( \vek{x} )\trn
        ( \vek{E} + \vek{v}\fl(\vek{x}))
        \right\rangle
    \end{align*}
    %
    for arbitrary $\vek{q}$ and $\vek{v}\fl \in \mathcal{E}\fl$.

    \item For a given $\vek{q}(\vek{x})$, the right hand side is minimized for 
    %
    \begin{align}
        \vek{v}\fl( \vek{x} )
        =
        -
        \int_{\mathbb{R}^d}
            \vek{\Gamma}\phs{0}( \vek{x} - \vek{y} ) 
            \vek{q}( \vek{y} )
        \de \vek{y}    
    \end{align}

    \item For this choice
    %
    \begin{align*}
        \left\langle
        \left( \vek{E} + \vek{v}\fl(\vek{x}) \right)^T
        \vek{L}\phs{0}
        \left( \vek{E} + \vek{v}\fl(\vek{x}) \right)
        \right\rangle
        = 
        \vek{E}\trn \vek{L}\phs{0} \vek{E} 
        - 
        \left\langle
        \vek{q}( \vek{x} )\trn
        \vek{v}\fl(\vek{x}))
        \right\rangle
    \end{align*}

    \item Hence
    %
    \begin{align*}
    \vek{E}^T \vek{L}^\mathrm{eff} \vek{E}
    & \leq 
    \vek{E}^T \vek{L}\phs{0} \vek{E}
    +
    \left\langle
    \vek{q}(\vek{x})^T 
    \left( \vek{L}\phs{0} - \vek{L}( \vek{x} )\right)^{-1}
    \vek{q}( \vek{x} )
    \right\rangle
    +
    \left\langle
    \vek{q}( \vek{x} )\trn
    \vek{v}\fl(\vek{x})
    \right\rangle
    +
    2 \left\langle
    \vek{q}( \vek{x} )\trn
    \vek{E}
    \right\rangle
    \\
    & =
    \vek{E}^T \vek{L}\phs{0} \vek{E}
    +
    \langle I_1 \rangle + \langle I_2 \rangle + \langle I_3 \rangle 
    \end{align*}

    \item Reverse inequality holds for $\vek{L}\phs{0} \preceq \vek{L}(\vek{x})$

\end{itemize}

\section{Hashin-Shtrikman-Willis bounds}

\begin{itemize}
    \item Considing the test polarization fields in the form
    %
    \begin{align*}
        \vek{q}( \vek{x}; \alpha )
        =
        \sum_{r=1}^{N}
        \chi\phs{r}( \vek{x}; \alpha ) 
        \vek{q}\phs{r}
    \end{align*}
    %
    leads to the choice~\eqref{eq:test_inclusion}.

    \item We shall proceed term by term and exchange enesmble and volume averaging
    
    \item First term
    %
    \begin{align*}
        \langle  
         \mathbb{E}\left[ I_1 \right]
        \rangle
        & =
        \left\langle
        \mathbb{E}\left[ 
        \left( 
            \sum_{r=1}^{N}
            \chi\phs{r}( \vek{x}; \alpha ) 
            \vek{q}\phs{r}
        \right)^T
        \left(  
            \sum_{s=1}^{N}
            \chi\phs{s}( \vek{x}; \alpha )
        \left( \vek{L}\phs{0} - \vek{L}\phs{s}\right)^{-1}
        \right)
        \left( 
            \sum_{t=1}^{N}
            \chi\phs{t}( \vek{x}; \alpha ) 
            \vek{q}\phs{t}
        \right)
        \right]
        \right\rangle
        \\
        & = 
        \sum_{r,s,t = 1}^{N}
        \left\langle
        \mathbb{E}\left[ 
            \chi\phs{r}( \vek{x}; \alpha ) 
            \chi\phs{s}( \vek{x}; \alpha )
            \chi\phs{t}( \vek{x}; \alpha ) 
        \right]
        \right\rangle
        \left( \vek{q}\phs{r} \right)^T
        \left( \vek{L}\phs{0} - \vek{L}\phs{s}\right)^{-1}
        \vek{q}\phs{t}
        \\
        & =
        \sum_{r=1}^{N}
        \left\langle
        \mathbb{E}\left[ 
            \chi\phs{r}( \vek{x}; \alpha ) 
        \right]
        \right\rangle
        \left( \vek{q}\phs{r} \right)^T
        \left( \vek{L}\phs{0} - \vek{L}\phs{r}\right)^{-1}
        \vek{q}\phs{r}
        \\
        & \stackrel{}{}=
        \sum_{r=1}^{N}
        c\phs{r}
        \left( \vek{q}\phs{r} \right)^T
        \left( \vek{L}\phs{0} - \vek{L}\phs{r}\right)^{-1}
        \vek{q}\phs{r}
    \end{align*}

    \item Second term
    %
    \begin{align*}
        \langle  
         \mathbb{E}\left[ I_2 \right]
        \rangle 
        & =
        \left\langle
        \mathbb{E} \left[
        \vek{q}( \vek{x}; \alpha )\trn
        \vek{v}\fl(\vek{x}; \alpha )
        \right]
        \right\rangle
        \\ 
        & = 
        \left\langle
        \mathbb{E} \left[
        \left( 
         \sum_{r=1}^N \chi\phs{r}( \vek{x}; \alpha ) 
         \vek{q}\phs{r}
        \right)\trn
        \left(
        -
        \sum_{s=1}^{N}
        \int_{\mathbb{R}^d}
        \vek{\Gamma}\phs{0}( \vek{x} - \vek{y} ) 
        \chi\phs{s}(\vek{y}; \alpha) 
        \vek{q}\phs{s}
        \right)
        \right]
        \right\rangle
        \\
        & =
        -
        \sum_{r,s=1}^{N}
        \left( \vek{q}\phs{r} \right)^T
        \left\langle
        \int_{\mathbb{R}^d}
        \mathbb{E} \left[
            \chi\phs{r}( \vek{x}; \alpha )
            \chi\phs{s}( \vek{y}; \alpha )
        \right]
        \vek{\Gamma}\phs{0}( \vek{x} - \vek{y} ) 
        \de\vek{y}
        \right\rangle
        \vek{q}\phs{s}
        \\
        & =
        - \sum_{r,s=1}^{N}
        \left( \vek{q}\phs{r} \right)^T
        \left\langle
        \int_{\mathbb{R}^d}
        S\phs{rs}( \vek{x}, \vek{y} )
        \vek{\Gamma}\phs{0}( \vek{x} - \vek{y} ) 
        \de\vek{y}
        \right\rangle
        \vek{q}\phs{s}
        =
        - \sum_{r,s=1}^{N}
        \left( \vek{q}\phs{r} \right)^T
        \vek{P}\phs{rs}
        \vek{q}\phs{s}
    \end{align*}

    \item Third term
    %
    \begin{align*}
        \langle  
         \mathbb{E}\left[ I_3 \right]
        \rangle 
        & =
        \left\langle
        \mathbb{E} \left[
        \vek{q}( \vek{x}; \alpha )\trn
        \vek{E}
        \right]
        \right\rangle
        = 
        \left\langle
        \mathbb{E} \left[
        \left( 
         \sum_{r=1}^N \chi\phs{r}( \vek{x}; \alpha ) 
         \vek{q}\phs{r}
        \right)\trn
        \vek{E} 
        \right]
        \right\rangle
        \\
        & =
        \sum_{r=1}^{N}
        \left\langle
        \mathbb{E} \left[
            \chi\phs{r}(\vek{x}; \alpha)
        \right] 
        \right\rangle
        \left( \vek{q}\phs{r} \right)^T
        \vek{E}     
        =
        \sum_{r=1}^{N}
        \left\langle
        S\phs{r}( \vek{x} )
        \right\rangle
        \left( \vek{q}\phs{r} \right)^T
        \vek{E}         
        =
        \sum_{r=1}^N
        c\phs{r}
        \left( \vek{q}\phs{r} \right)^T
        \vek{E}  
    \end{align*}

    \item Matrices $\vek{P}\phs{rs}$ chapture interactions among phases
    
    \item For statistically uniform media
    %
    \begin{align*}
        \vek{P}\phs{rs}
        =
        \left\langle
        \int_{\mathbb{R}^d}
        S\phs{rs}( \vek{x} - \vek{y} )
        \vek{\Gamma}\phs{0}( \vek{x} - \vek{y} ) 
        \de\vek{y}
        \right\rangle
        =
        \left\langle
        \int_{\mathbb{R}^d}
        S\phs{rs}( \vek{y} )
        \vek{\Gamma}\phs{0}( \vek{y} ) 
        \de\vek{y}
        \right\rangle
        =
        \int_{\mathbb{R}^d}
        S\phs{rs}( \vek{y} )
        \vek{\Gamma}\phs{0}( \vek{y} ) 
        \de\vek{y}
    \end{align*}

    \item Statistically uniform media, it can be shown by Fourier transofrm techqniues that 
    %
    \begin{align}
        \vek{P}\phs{rs}
        =
        \left( \delta_{rs} c\phs{r} - c\phs{r} c\phs{s}\right)
        \frac{1}{4 \pi}
        \int_{S^3}
        \widehat{\vek{\Gamma}}\phs{0}
        ( \vek{\xi} )
        \de \vek{\xi}
        =
        \left( \delta_{rs} c\phs{r} - c\phs{r} c\phs{s}\right)
        \vek{P}_\mathrm{sphere}
    \end{align}
    %
    otherwise, the matrices must be evaluated by a numerical quadrature.

    \item In conclusion, 
    %
    \begin{align*}
        \vek{E}^T \vek{L}^\mathrm{eff} \vek{E}
        & \leq 
        \vek{E}^T \vek{L}\phs{0} \vek{E}
        +
        \sum_{r=1}^{N}
        c\phs{r}
        \left( \vek{q}\phs{r} \right)^T
        \left( \vek{L}\phs{0} - \vek{L}\phs{r}\right)^{-1}
        \vek{q}\phs{r}
        \\
        & -
        \sum_{r,s=1}^{N}
        \left( \vek{q}\phs{r} \right)^T
        \vek{P}\phs{rs}
        \vek{q}\phs{s}
        +
        2 
        \sum_{r=1}^N
        c\phs{r}
        \left( \vek{q}\phs{r} \right)^T
        \vek{E}  
    \end{align*}

    \item For given $\vek{L}\phs{0}$, the right-hand side expression must be minimized wrt to $\vek{q}\phs{r}$, yielding for $N=2$
    %
    \begin{align*}
        \begin{bmatrix}
            \vek{P}\phs{11} - c\phs{1} \left( \vek{L}\phs{0} - \vek{L}\phs{1}\right)^{-1} &
            \vek{P}\phs{12}
            \\
            \vek{P}\phs{21}
            &
            \vek{P}\phs{22} - c\phs{2} \left( \vek{L}\phs{0} - \vek{L}\phs{1}\right)^{-1}
        \end{bmatrix}
        \begin{bmatrix}
            \vek{p}\phs{1} \\
            \vek{p}\phs{2}
        \end{bmatrix}
        =
        \begin{bmatrix}
            c\phs{1} \vek{E}
            \\
            c\phs{2} \vek{E}
        \end{bmatrix}
    \end{align*}
    %
    where $\vek{p}\phs{r}$ are optimal phase polarization

    \item Inserting the optimal polization into the upper bound yields
    %
    \begin{align*}
        \vek{E}^T \vek{L}^\mathrm{eff} \vek{E}
        & \leq 
        \vek{E}^T \vek{L}\phs{0} \vek{E}
        +
        \sum_{r=1}^N
        c\phs{r}
        \left( \vek{p}\phs{r} \right)^T
        \vek{E}  
    \end{align*}

    \item Must be finally optimized with respect to $\vek{L}\phs{0}$ to obtain the tightest estimate
\end{itemize}

\section{Example: Isotropic suspension of spheres in istotropic matrix}

\Reference

\Acknowledgement 

\end{document}
 